<!-- % Микропроцессорные системы -->
<!-- pandoc -f markdown -t pdf --pdf-engine=wkhtmltopdf --pdf-engine-opt=--page-size --pdf-engine-opt=a4 --pdf-engine-opt=--minimum-font-size --pdf-engine-opt=20 --pdf-engine-opt=--page-offset --pdf-engine-opt=1 --pdf-engine-opt=-q -s -o readme.pdf readme.md -->

1. [Общие вопросы](./общие_вопросы): разное
1. [Аппаратные средства](./аппаратные_средства): аппаратное обеспечение микропроцессорных систем
1. [Шины](./шины): шины передачи данных
1. [ЭВМ](./эвм): внутреннее устройство ЭВМ
1. [ОС](./ос): работа операционных систем
1. [Сети](./сети): сетевое взаимодействие
1. [HDL](./hdl): языки описания логических устройств

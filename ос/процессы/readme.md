---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Процессы

Микропроцессорные системы

## Виртуализация

Задача ОС
- предоставить абстракцию процесса
  * выполняется в собственном адресном пространстве
  * имеет эксклюзивный доступ к процессору
  * имеет доступ к диску и периферийным устройствам
* изолировать процессы
  * изолировать данные в памяти
  * разделить процессорного времени между процессами
  * права и политики доступа

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## Вызов программы

| ОС  | Процесс  |
|:--|:--|
| добавление процесса в список процессов |   |
| выделение памяти процесса |   |
| загрузка программы в память |   |
| заполнение стека аргументами |   |
| очистка регистров |   |
| вызов функции `main()` |   |
|  | выполнение `main()` |
|  | завершение программы вызовом `return` |
| очистка памяти процесса  |   |
| удаление процесса из списка процессов  |   |

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## Переход между программами

| ОС  | HW  | Процесс  |
|:--|:--|:--|
|   |   | выполнение `main()` процесса А  |
|   |   | сигнал `trap`  |
|   | сохранение регистров процесса А в стек ОС  |   |
| замена памяти и регистров А на Б  |  |   |
|  | восстановление регистров из стека ОС  |   |
|  |   | возвращение в `main()` процесса Б  |

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## First In First Out (FIFO)

![bg right:30% width:100%](./images/fifo.png)

- Выполнение процессов не прерывается
- Выполнение в порядке очереди FIFO

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## Shortest Job First (SJF)

![bg right:30% width:100%](./images/sjf.png)

- Выполнение процессов не прерывается
- Заранее известно время выполнения процесса
- Список процессов известен заранее

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## Shortest Time-to-Completion First (STCF)

![bg right:30% width:100%](./images/stcf.png)

- Заранее известно время выполнения процесса

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## Round Robin (RR)

![bg right:30% width:100%](./images/rr.png)

- Одинаковое процессорное время
- Малое время до запуска
- Не зависит от времени прихода задачи
- Освобождение слота при выполнении IO

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## Multi-Level Feedback Queue (MLFQ)

![bg right:30% width:100%](./images/mlfq.png)

- Выполняется процесс с большим приоритетом
- При одинаковом приоритете используется RR
- Максимальный приоритет при начале выполнения
- Приоритет снижается при использовании одного слота
- Приоритет не меняется при вызове IO до окончания слота

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## MLFQ boost

![bg right width:100%](./images/mlfq-boost.png)

- Через определённое время приоритет всех процессов выставляется максимальным

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## MLFQ game tolerance

![bg right width:100%](./images/mlfq-gt.png)

- Частичное использование слота

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## MLFQ game tolerance

![bg right width:100%](./images/mlfq-gt.png)

- Учёт частичного использования слота при вызове IO

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## Лотерея

- Процессам выделяются "билеты" пропорционально приоритету
- Следующий процесс выбирается случайным образом из всех билетов

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## Многопроцессорные планировщики

![bg width:80%](./images/mp_bad.png)
![bg width:80%](./images/mp_better.png)

> https://pages.cs.wisc.edu/~remzi/OSTEP/

## Планировщики Linux

- O(n)
- O(1)
- Completely Fair Scheduler (CFS)
- SCHED_DEADLINE
- Brain F**k Scheduler (BFS)

> https://pages.cs.wisc.edu/~remzi/OSTEP/

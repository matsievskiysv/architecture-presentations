---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Сборка и выполнение программ

Микропроцессорные системы

## Виртуальные машины

![bg right width:100%](./images/qemu.png)

- Эмуляция архитектуры процессора
- Эмуляция устройств
- Возможность использования другой ОС или работа без OS (bare metal)

## Объявление, определение и инициализация

- Объявление – информирование о типе переменной или аргументах функции
- Определение – выделение памяти для переменной
- Инициализация – задание значения переменной

## Файлы заголовков

<div class="twocols">

- Перед использованием символов (переменных или функций) в коде, они должны быть объявлены
* Объявления функций выносят в файлы заголовков `.h`

<p class="break"></p>

```c
int foo(int a);

int main(void) {
  int a = foo(1);
}

int foo(int a) {
  return a + 1;
}
```

</div>

## Структура проекта

```
.
├── include
│   └── foo.h
├── main.ld
├── Makefile
└── src
    ├── bar.c
    ├── foo.c
    └── main.c
```

## Файл `src/main.c`

```c
#include "foo.h"

extern int b;
static int c = 4;

int
main(void)
{
	int x = a + b + c;
	x = factorial(x);

	for (;;)
		;

	return 0;
}
```

## Файл `include/foo.h`

```c
#pragma once

extern int a;

int factorial(int n);
```

## Компиляция `src/main.c`

```bash
riscv64-unknown-elf-gcc -c -Iinclude \
	-march=rv32i -mabi=ilp32 \
	-O0 -Wall -Wextra -pedantic \
	-o src/main.o src/main.c
```

- `-c` – компиляция без композиции
- `-I<dir>` – путь к файлам заголовков
- `-march=rv32i -mabi=ilp32` – параметры процессора
- `-O0` – без оптимизации
- `-Wall -Wextra -pedantic` – с предупреждениями
- `-o <name>` – выходной файл

## Результат компиляции `src/main.c`

```bash
riscv64-unknown-elf-objdump -wD src/main.o
```

```
Disassembly of section .text:
00000000 <main>:
   0:   fe010113                addi    sp,sp,-32
   4:   00112e23                sw      ra,28(sp)
   8:   00812c23                sw      s0,24(sp)
   c:   02010413                addi    s0,sp,32
  10:   000007b7                lui     a5,0x0
  14:   0007a703                lw      a4,0(a5) # 0 <main>
  18:   000007b7                lui     a5,0x0
  1c:   0007a783                lw      a5,0(a5) # 0 <main>
  20:   00f70733                add     a4,a4,a5

Disassembly of section .sdata:
00000000 <c>:
   0:   0004                    0x4
```

## Файл `src/foo.c`

```c
#include "foo.h"

int a = 1;

int factorial(int n) {
  if (n <= 1)
    return 1;
  else
    return factorial(n - 1);
}
```

## Результат компиляции `src/foo.c`

```bash
riscv64-unknown-elf-objdump -wD src/foo.o
```

```
Disassembly of section .text:
00000000 <factorial>:
   0:   fe010113                addi    sp,sp,-32
   4:   00112e23                sw      ra,28(sp)
   8:   00812c23                sw      s0,24(sp)
   c:   02010413                addi    s0,sp,32
  10:   fea42623                sw      a0,-20(s0)
  14:   fec42703                lw      a4,-20(s0)
  18:   00100793                li      a5,1
  1c:   00e7c663                blt     a5,a4,28 <.L2>
  20:   00100793                li      a5,1

Disassembly of section .sdata:
00000000 <a>:
   0:   0001                    nop
```

## Файл `src/bar.c`

```c
int b = 2;
int d[10] = {0};
```

## Результат компиляции `src/bar.c`

```bash
riscv64-unknown-elf-objdump -wD src/bar.o
```

```
Disassembly of section .bss:
00000000 <d>:

Disassembly of section .sdata:
00000000 <b>:
   0:   0002                    c.slli64        zero
```

## Скрипт компоновщика `main.ld`

```ld
OUTPUT_FORMAT("elf32-littleriscv", "elf32-bigriscv", "elf32-littleriscv")
OUTPUT_ARCH(riscv)
ENTRY(main)

MEMORY {
	ROM	(rx)	: ORIGIN = 0x10000000,	LENGTH = 64k
	ROMW	(rwx)	: ORIGIN = 0x10010000,	LENGTH = 64k
	RAM	(rwx)	: ORIGIN = 0x20000000,	LENGTH = 512k
}

SECTIONS {
	.text : { *(.text) } > ROM
	.data : { *(.sdata) } > ROMW
	.bss  : {
		*(.bss)
		_stack_end = ABSOLUTE(.);
		. = . + 0x100;
		. = ALIGN(0x100);
		_stack_start = ABSOLUTE(.);
	} > RAM
}
```

## Компоновка

```bash
riscv64-unknown-elf-gcc -march=rv32i -mabi=ilp32 \
	-O0 -Tmain.ld \
	-nostdlib -nostartfiles -ffreestanding \
	-Wall -Wextra -pedantic \
	-o main.elf \
	src/bar.o src/foo.o src/main.o
```

- `-T<linker_script>` – путь к скрипту компоновщика
- `-nostdlib -nostartfiles -ffreestanding` – без стандартных библиотек

## Результат компоновки

```bash
riscv64-unknown-elf-objdump -wD main.elf
```

```
Disassembly of section .text:
10000000 <factorial>:
10000000:       fe010113                addi    sp,sp,-32
10000004:       00112e23                sw      ra,28(sp)
10000008:       00812c23                sw      s0,24(sp)
1000000c:       02010413                addi    s0,sp,32
10000010:       fea42623                sw      a0,-20(s0)
10000014:       fec42703                lw      a4,-20(s0)
10000018:       00100793                li      a5,1
1000001c:       00e7c663                blt     a5,a4,10000028 <factorial+0x28>
10000020:       00100793                li      a5,1
10000024:       0180006f                j       1000003c <factorial+0x3c>

10000050 <main>:
10000050:       fe010113                addi    sp,sp,-32
10000054:       00112e23                sw      ra,28(sp)
```

## Результат компоновки

```bash
riscv64-unknown-elf-objdump -wD main.elf
```

```
Disassembly of section .data:
10010000 <b>:
10010000:       0002                    c.slli64        zero
10010004 <a>:
10010004:       0001                    nop
10010008 <c>:
10010008:       0004                    0x4

Disassembly of section .bss:
20000000 <d>:
```

## Варианты компоновки

| Статическая  | Динамическая  |
|:--|:--|
| Программа и библиотеки компилируются в единый бинарный файл  | Библиотеки компилируются в специальном формате, подключаемом при работе  |
| Большой размер файла  | Малый размер файла  |
| Быстрый запуск  | Требуется время на подключение библиотек  |
| Библиотека используется только одной программой  | Библиотека используется многими программами  |
| При обновлении библиотек надо перекомпилировать все файлы  | Возможно раздельное обновление  |

## Точка входа программы

- Указатель на место, откуда начинается программа
* При работе без ОС определяется архитектурой процессора
* При загрузке из ОС может быть изменён в скрипте компоновщика

## Правила вызова функций RISCV

- Первые аргументы передаются в 8 целочисленных регистрах (`a0`-`a7`) и в 8 регистрах с плавающей точкой (`fa0`-`fa7`)
- Остальные аргументы передаются через стек. `sp` указывает на первый аргумент в стеке
- Результат функции записывается в 2 целочисленных регистрах (`a0`-`a1`) и в 2 регистрах с плавающей точкой (`fa0`-`fa1`)

> https://riscv.org/wp-content/uploads/2015/01/riscv-calling.pdf

#include "foo.h"

int a = 1;

int factorial(int n) {
  if (n <= 1)
    return 1;
  else
    return factorial(n - 1);
}

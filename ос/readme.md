<!-- % Микропроцессорные системы -->
<!-- pandoc -f markdown -t pdf --pdf-engine=wkhtmltopdf --pdf-engine-opt=--page-size --pdf-engine-opt=a4 --pdf-engine-opt=--minimum-font-size --pdf-engine-opt=20 --pdf-engine-opt=--page-offset --pdf-engine-opt=1 --pdf-engine-opt=-q -s -o readme.pdf readme.md -->

# Операционные системы
## Главы

1. [Сборка и выполнение программ](./сборка_и_выполнение_программ)
	1. Объявление, определение и инициализация
	1. Точка входа в программу
	1. Компиляция программы
	1. Композиция программы
	1. Статические и динамические библиотеки
	1. Конвенция вызова функций
1. [Процессы](./процессы)
	1. Виртуализация
	1. Сохранение регистров
	1. Планировщики процессов
		1. First In First Out (FIFO)
		1. Shortest Job First (SJF)
		1. Shortest Time to Completion First (STCF)
		1. Round Robin
		1. Учёт обращений к памяти
		1. Multi-Level Feedback Queue (MLFQ)
		1. Proportional Share
1. Многопоточность
	1. Потоки
	1. Замки
	1. Deadlock
	1. Когерентность кешей
	1. Структуры данных с параллельным доступом
	1. Структуры данных без замков
	1. Передача данных от Производителя потребителю
	1. Сопрограммы
	1. Асинхронные функции
1. Файловые системы
	1. Файлы и папки
	1. Иноды
	1. Организация доступа
	1. Запись и чтение
	1. Кеширование и буферизация
	1. Локальность
	1. Журналирование
	1. Контрольные суммы
	1. Коды Хэмминга
	1. Распределённые файловые системы
	1. RAID массивы
1. IPC (Межпроцессное сообщение)
	1. Pipes
	1. Named pipes
	1. Общая память
	1. UDP
	1. TCP
	1. MPI
	1. PETSc/SLEPc


## Литература

1. [Сборка и выполнение программ](./сборка_и_выполнение_программ)
	* https://gcc.gnu.org/onlinedocs/gcc-11.2.0/gcc/
	* https://ftp.gnu.org/old-gnu/Manuals/ld-2.9.1/html_chapter/ld_3.html
1. [Процессы](./процессы)
	* [Operating Systems: Three Easy Pieces](https://pages.cs.wisc.edu/~remzi/OSTEP/)

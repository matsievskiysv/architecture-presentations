---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Shell. Основы

Микропроцессорные системы

## Мотивация

- CLI интерфейс требует меньше ресурсов
- легко сделать CLI интерфейс для программы
- команды легко связать вместе
- позволяют автоматизировать повторяющиеся действия
- используется повсеместно

## Команды

```bash
/path/to/program -a -b -c --opt1 --opt2 optarg arg1 arg2
program -d -e -f --opt1 --opt2 arg3 arg4 arg5
```

- `/path/to/program`: путь программы
- `-a`: короткая опция (флаг)
- `--opt1`: длинная опция
- `--opt2 arg`: длинная опция с аргументом
- `arg1`: аргумент

```bash
find /etc -name "*.conf" -printf '%s\t%f\n'
git -C beadpullxx log -n 3 --graph
```

## Вывод на экран

```bash
echo hello world
```

- `-n` не добавлять символ `\n` в конце строки
- `-e` выводить специальные символы

```bash
for col in $(seq 31 36); do
    echo -e "\e[${col}mSample Text\e[0m"
done
```

## Абсолютный и относительный путь

```bash
pwd
```

- `/path/to/file`: абсолютный путь
- `./relative/to/file`: путь относительно директории `$PWD`

## Перемещение по папкам

<div class="twocols">

```bash
pwd      # /
cd /a/1
pwd      # /a/1
cd /b/2
pwd      # /b/2
cd ..
pwd      # /b
cd .
pwd      # /b
cd ./3
pwd      # /b/3
cd ../c/1
pwd      # /c/1
cd ../1/./../../a/./../a/./1/.
```

<p class="break"></p>

```
├── a
│   ├── 1
│   ├── 2
│   └── 3
├── b
│   ├── 1
│   ├── 2
│   └── 3
└── c
    ├── 1
    ├── 2
    └── 3
```

</div>

## Операции с директориями

- `mkdir`: создание директории
   ```bash
   mkdir dir1 dir2 dir3
   ```
- `rmdir`: удаление директории
   ```bash
   rmdir dir1 dir2 dir3
   ```
- `ls`: печать содержимого директории
   ```bash
   ls
   ```

## Операции с файлами

- `touch`: создание файла
   ```bash
   touch filename1 filename2 filename3
   ```
- `rm`: удаление файла
   ```bash
   rm filename1 filename2 filename3
   ```
- `cat`: печать содержимого файла
   ```bash
   cat filename1 filename2 filename3
   ```

## Перемещение и копирование

- `mv`: перемещение
   ```bash
   mv fromfile tofile
   mv fromdir todir
   ```
- `cp`: копирование
   ```bash
   cp fromfile tofile
   cp -r fromdir todir
   ```

## Редактирование файлов

```bash
nano main.c
```
- <kbd>Ctrl+o</kbd>: сохранить файл
- <kbd>Ctrl+x</kbd>: выйти из редактора
- <kbd>Ctrl+6</kbd>: выделение текста
- <kbd>Alt+6</kbd>: копирование текста
- <kbd>Ctrl+k</kbd>: вырезание текста
- <kbd>Ctrl+u</kbd>: вставка текста


## Входы и выходы программ

- Стандартные в(ы)ходы программ
   * Стандартный вход `stdin`
   * Стандартный выход `stdout`
   * Стандартный выход ошибки `stderr`

## Перенаправление выходов программ

- Вывод в файл
   ```bash
   echo some string > /tmp/myfile
   ```
- Вход из файла
   ```bash
   cat < /tmp/myfile
   ```

## Перенаправление от программы к программе

```bash
echo some string > /tmp/myfile
wc -c < /tmp/myfile
echo some string | wc -c
```

## Перенаправление от программы к программе

```bash
echo some string > /tmp/myfile
wc -c < /tmp/myfile
echo some string | wc -c
```

```bash
echo -e '4\n5\n1\n3\n4\n2\n2\n5' | sort -n | uniq \
   | awk '{if ($1 % 2 == 0) {print $1 * 2} else {print $1 - 1}}'
```

## Переменные

```bash
a=1
b=string
c="this is"

echo $c $a $b
echo $c $a $b | wc
```

## Алгебра

```bash
a=1
b=2

echo $((a * b + b))
```

## Запись вывода программы в переменную

```bash
mytime=$(date)
echo Точное время $mytime
```

```bash
a=1
b=2

c=$((a * b + b))
echo "$a * $b + $b = $c"
```

## Условные операторы

```bash
myvar=blue

if [ $myvar = red ]
then
    echo Color is red
else
    echo Color is not red
fi
```

## Циклы

```bash
for i in 1 2 3
do
   echo This number is $i
done
```

```bash
i=1
while [ $i -lt 5 ]
do
   echo This number is $i
   i=$((i + 1))
done
```

## Функции

```bash
seq 3 10
```

```bash
myseq() {
    i=$1
    while [ $i -le $2 ]
    do
       echo $i
       i=$((i + 1))
    done
}

myseq 3 10
```

## Документация

```bash
man curl
man jq
```

```bash
sed -h
ls --help
nmcli help
```

## Установка/удаление программ

```bash
sudo apt update
sudo apt upgrade
sudo apt install emacs
sudo apt remove vim
```

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity circuit_testbench is
end;

architecture rtl of circuit_testbench is
  component circuit is
    port (a, b, c		: in std_logic;
          output, carry		: out std_logic);
  end component;
  signal input		: std_logic_vector(2 downto 0)	:= (others => '0');
  signal output		: std_logic_vector(1 downto 0)	:= (others => '0');
begin
  dut : circuit
    port map(
      a => input(0),
      b => input(1),
      c => input(2),
      output => output(0),
      carry => output(1));
  process is
  begin
    for i in 0 to 8 loop
      input <= std_logic_vector(to_unsigned(i, input'length));
      wait for 10 ns;
    end loop;
  end process;
end;

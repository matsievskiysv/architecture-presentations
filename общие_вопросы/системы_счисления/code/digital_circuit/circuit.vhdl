library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity circuit is
  port (a, b, c		: in std_logic;
        output, carry	: out std_logic);
end;

architecture circuit of circuit is
  signal i1, i2, i3, i4: std_logic := '0';
begin
  i1 <= a and b;
  i2 <= a xor c;
  i3 <= b or c;
  i4 <= i1 or i2;
  output <= i4 and i3;
  carry <= i4 and i3 and b;
end;

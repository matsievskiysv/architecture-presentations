#!/usr/bin/env python3

from sys import argv, exit

if len(argv) < 2:
    exit(f"Usage: {argv[0]} <int>")
print(bin(int(argv[1])))

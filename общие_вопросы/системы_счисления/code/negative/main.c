#include <stdio.h>
#include <stdlib.h>
#define BITS 8

int main(int argc, char *argv[]) {
	if (argc < 2) {
		printf("Usage: ./main <int>\n");
		goto error;
	}

	printf("0b");
	for (int i=1; i <= BITS; i++) {
		printf("%d", !!(atoi(argv[1]) & (1 << (BITS - i))));
	}
	printf("\n");
	return 0;
error:
	return 1;
}

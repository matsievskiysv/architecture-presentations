---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Git

Микропроцессорные системы

## Мотивация

- система контроля версий файлов и проектов
- изначально разрабатывалась для ядра Linux
- де-факто стандарт при разработке ПО
- примерно переводится как «вздорный старик» (подразумевается Линус Торвальдс)


## Представление изменений

![width:100% bg right:60%](./images/graph.svg)

- репозиторий (repository, repo)
- ветка (branch)
- изменение (commit)
- главная ветка (master, main, trunc ...)

## Состояние изменений

![width:100% bg right:60%](./images/staging.svg)

- remote: удалённый репозиторий
- local: локальный репозиторий
- staged: выбранные изменения
- unstaged: не выбранные изменения
- stash: спрятанные изменения

## Создание репозитория

Новый проект
```bash
git init
```

Существующий проект
```bash
git clone [--recursive] <url> [folder_name]
```

- `--recursive`: рекурсивное клонирование подпроектов
- `folder_name`: название папки проекта

## Создание репозитория код

```code
mkdir project
cd project
git init
```
## Добавление изменений

![width:100% bg right:50%](./images/add.svg)

Добавление файла/изменений в stage
```bash
git add <file>
```
Удаление файла из stage
```bash
git rm <file>
```
Удаление изменений из stage
```bash
git checkout -- <file>
```

## Добавление изменений код

```bash
echo hello world > file.txt
uname -a > machine.txt
git add file.txt machine.txt
git commit -m 'Initial commit'
echo goodbye world >> file.txt
git commit -a -m 'Say goodbye'
git rm machine.txt
git commit -a -m 'Remove machine'
truncate -s 0 file.txt
git checkout -- file.txt
```

## Состояние репозитория

Состояние репозитория
```bash
git status
```
Лог изменений
```bash
git log [--graph] <branch_list>
```
- `--graph`: представление в виде графа
- `<branch_list>`: список веток для отображения

История файла
```bash
git blame <file>
```

## Операции с ветками

![width:100% bg right:50%](./images/branch.svg)

Создание веток
```bash
git branch <name>
```
Удаление веток
```bash
git branch -d <name>
```
Переход к ветке
```bash
git checkout <name>
```

Добавление тега
```bash
git tag <name>
```

## Операции с ветками код

```bash
git branch number_list
echo '- learn git' > todo.md
git add todo.md
git commit -a -m 'Add todo list'
git tag 'v1.0'
git checkout number_list
seq 100 > number.list
git add number.list
git commit -a -m 'Add number list'
git log --graph master number_list
```

## Разница между ревизиями

Локальные изменения
```bash
git diff
```
Разница между ветками
```bash
git diff <branch_1> <branch_2>
```
Сохранение изменений в файл
```bash
git diff > <patch>
```
Применение изменений из файла
```bash
git apply <patch>
```

## Разница между ревизиями код

```bash
git diff number_list master
git checkout master
echo '- write code' >> todo.md
git diff
git diff > /tmp/todo.patch
git checkout todo.md
git apply /tmp/todo.patch
```

## Временное сохранение изменений

Сохранение изменений
```bash
git stash
```
Восстановление изменений
```bash
git pop
```

## Временное сохранение изменений код

```bash
git checkout todo.md
git apply /tmp/todo.patch
git diff
git stash
git checkout number_list
seq 101 105 >> number.list
git commit -a -m 'Update list'
git checkout master
git stash pop
git diff
git commit -a -m 'Update todo'
```

## Слияние веток

![width:100% bg right:50%](./images/merge.svg)

Слияние веток
```bash
git merge <branch>
```
Отмена слияния при конфликте
```bash
git merge --abort
```

## Слияние веток код

```bash
git checkout master
git merge number_list
git branch edit_numbers
sed -i 's/55/51/g' number.list
git commit -a -m 'Update number list'
git checkout edit_numbers
sed -i 's/55/52/g' number.list
git commit -a -m 'Update number list'
git checkout master
git diff master edit_numbers
git merge edit_numbers
git status
nano number.list
git status
git add number.list
git commit -m 'Merge changes'
git log --graph
```

## Смена базового изменения

![width:100% bg right:50%](./images/rebase.svg)

Смена базового изменения
```bash
git rebase <branch>
```
Отмена действия при конфликте
```bash
git rebase --abort
```
Продолжение действия после разрешения конфликта
```bash
git rebase --continue
```

## Смена базового изменения код

```bash
git checkout v1.0
git checkout -b rebase
date -Iseconds > timestamp
git add timestamp
git commit -m 'Add timestamp'
git checkout master
git rebase rebase
nano number.list
git add number.list
git rebase --continue
git log --graph
```

## Добавление изменения из другой ветки

![width:100% bg right:50%](./images/cherrypick.svg)

Добавление изменения
```bash
git cherry-pick <commit>
git cherry-pick <from>..<to>
```

## Добавление изменения из другой ветки код

```bash
git branch feature
echo 1 >> number.list
git commit -a -m 'Update number list'
echo 2 >> number.list
git commit -a -m 'Update number list'
git checkout feature
echo 1 >> number.list
git commit -a -m 'Update number list'
echo '- do homework' >> todo.md
git commit -a -m 'Update todo list'
git tag cherry
echo 2 >> number.list
git commit -a -m 'Update number list'
git checkout master
git log --graph master feature
git cherry-pick cherry
git log --graph master feature
```

## Взаимодействие с удалённом сервером

Загрузка информации с удалённого сервера
```bash
git fetch <remote>
```
Загрузка изменений с ветки на удалённом сервере
```bash
git pull <remote> <branch>
```
Загрузка изменений в ветку на удалённом сервере
```bash
git push <remote> <branch>
```

## Интерфейс удалённого сервера

- fork: создание копии репозитория на сервере
- pull request (PR) / merge request (MR) : слияние кода в копию репозитория на сервере

## Подпроекты

Для управления подпроектами git существует 2 механизма:

- `git submodule`
- `git subtree`

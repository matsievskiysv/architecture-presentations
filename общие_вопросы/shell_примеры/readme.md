---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Shell. Примеры

Микропроцессорные системы

## Создание множества директорий

<div class="twocols">

```bash
mkdir -p {1,2}/{a,b,c}/{x..z}
```

<p class="break"></p>

```
├── 1
│   ├── a
│   │   ├── x
│   │   ├── y
│   │   └── z
│   ├── b
│   │   ├── x
│   │   ├── y
│   │   └── z
│   └── c
│       ├── x
│       ├── y
│       └── z
└── 2
    ├── a
    │   ├── x
    │   ├── y
    │   └── z
    ├── b
    │   ├── x
    │   ├── y
    │   └── z
    └── c
        ├── x
        ├── y
        └── z
```

</div>

## Вывод на экран начала/конца файла

```bash
head -n 10 file
tail -n 5 file
```

## Подсчёт количества строк в файлах

```bash
find . -name "*.cxx" -exec wc -l {} \+
```

## Преобразование рисунков

```bash
parallel convert {} -rotate 90 -colorspace GRAY {.}_gray.jpg ::: *.jpg
```

## Удаление метаданных фотографий

```bash
exiftool -All='' *
```

## Склейка PDF файлов

```bash
pdftk A=file1.pdf B=file2.pdf cat A1-4 Bodd A5-end output out.pdf
```

## Поиск файлов и преобразование списка

<div class="twocols">

```bash
find /folder -maxdepth 2 -type f -size +200M -size -10G \
    -printf '%s %f\n' \
    | sort -nr | head -n 5 \
    | awk '{printf "%s\t:\t%dMiB\n", $2, $1/1024/1024}'
```

<p class="break"></p>

```
f1 :       5240MiB
f2 :       3234MiB
f3 :       3216MiB
f4 :       3025MiB
f5 :       2870MiB
```

</div>

## Замена строк в файлах

```bash
sed -i 's/red/green/g' file1 file2 file3
```

## Поиск фразы в текстовых файлах

```bash
grep -R decoder .
```

## Поиск фразы в PDF файлах

```bash
pdfgrep -R 'ускоритель электронов' .
```

## Поиск и замена регулярных выражений

| ![center width:15cm](./images/regexp-1.png)  | ![center width:12cm](./images/regexp-2.png)  |
|:-:|:-:|
|   |   |

> https://regexr.com/

## Изменение разделителя в CSV файлах

```bash
sed -i 's/\t/,/g' data.csv
```

## Получение колонки из CSV файла

```bash
cut -d, -f3 data.csv | uniq | sort -n
```

## Изменение размера файла

```bash
truncate -s 1M file.bin
```

## Подготовка файла к записи на Flash накопитель

```bash
truncate -s 1M nor.img
tr '\0' '\377' < nor.img > _nor.img && mv _nor.img nor.img
dd if=data.csv of=nor.img conv=notrunc
```

## Автоматическое сохранение файлов по расписанию

```bash
do_backup() {
    filename=$(date -I).zip
    zip -ry $filename /path/to/my/files/
    curl -u user:password -T $filename -X PUT https://mybackupserver.com/backups
}
```

```bash
crontab - <<< '21 3 * * 1 do_backup_script'
```

## Периодическая проверка доступности сайта

```bash
testsite() {
    while ! $(curl -s $1)
    do
        echo 'Site is down'
        sleep 3
    done
    echo 'Site is up'
}

testsite https://ya.ru
```

## Скачать все файлы с расширением по ссылке

```bash
wget -nd -A ppt,pptx,pdf https://some.site
```

## Узнать свой IP адрес

```bash
ip -br address
curl http://ifconfig.me
```

## Узнать погоду

```bash
curl https://api.open-meteo.com/v1/forecast\?latitude\=55.45\&longitude\=37.37\&current\=temperature_2m \
    | jq '.current.temperature_2m'
```

## Скачать видео из YouTube

```bash
yt-dlp https://www.youtube.com/watch?v=dQw4w9WgXcQ
```

## Преобразовать страницу сайта в другой формат

```bash
curl -s -L https://ru.wikipedia.org/wiki/Служебная:Случайная_страница -o file.html
pandoc -f html -t markdown file.html file.md
```

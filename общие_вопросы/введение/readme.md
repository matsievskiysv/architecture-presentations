---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Введение

Микропроцессорные системы

<!--
Что такое микропроцессор?
Где используется?
Как устроен микропроцессор?
-->

## Чарльз Бэббидж и Ада Лавлейс

| ![Ada Lovelace](images/Ada_Lovelace.jpg) | ![Charles Babbage](images/Charles_Babbage.jpg) |
|:-:|:-:|
| Ада Лавлейс | Чарльз Бэббидж |

> [https://ru.wikipedia.org/wiki/Лавлейс,_Ада](https://ru.wikipedia.org/wiki/Лавлейс,_Ада)
> [https://ru.wikipedia.org/wiki/Бэббидж,_Чарлз](https://ru.wikipedia.org/wiki/Бэббидж,_Чарлз)

<!--
1830е-1870е - аналитическая машина Чарльза Бэббиджа, работающая на паровой машине.
Ада описывает алгоритм вычисления чисел Бернулли.
Язык программирования - Ада.
-->

## Intel 4004 (1971 г.)

![bg right height:80%](images/intel_4004.png)

  - 2.300 транзисторов
  - 108 кГц
  - 10 μm

<!--
Первый микропроцессор имел такие характеристики
-->

## Intel Kaby Lake (2020 г.)

![bg right width:90%](images/KabyLakeDie.jpg)

  - 4.400.000.000 транзисторов
  - 4.5 ГГц
  - 14 nm

<!--
Современные микропроцессоры имеют такие характеристики
-->

## Техпроцесс

| Техпроцесс | Год | Техпроцесс | Год | Техпроцесс | Год |
|:--|:--:|:--|:--:|:--|:--:|
| 10 µm | 1971 | 250 nm | 1996 | 14 nm | 2014 |
| 6 µm | 1974 | 180 nm | 1999 | 10 nm | 2016 |
| 3 µm | 1977 | 130 nm | 2001 | 7 nm | 2018 |
| 1.5 µm | 1981 | 90 nm | 2003 | 5 nm | 2020 |
| 1 µm | 1984 | 65 nm | 2005 | 3 nm | 2022 |
| 800 nm | 1987 | 45 nm | 2007 | 2 nm | 2024 |
| 600 nm | 1990 | 32 nm | 2009 | | |
| 350 nm | 1993 | 22 nm | 2012 | | |

<!--
Прогресс в технологии изготовления процессоров
-->

## Закон Мура

![bg height:80%](images/moore.png)

<!--
Империческая закономерность. Увеличение числа транзисторов в 2 раза каждые 2 года.
-->

## Аналоговые и цифровые сигналы

![height:10cm center](images/analog_digital.jpg)

<!--
Отличие аналогового сигнала от цифрового. В чём разница?
Какой используется в компьютерах?
-->

## Аналоговые и цифровые сигналы

  - Аналоговый сигнал
    $i_E = \frac{I_S}{\alpha_F}\left[\exp{\left(\frac{v_{BE}}{V_T}\right)}-1\right]$
  * Цифровой сигнал
    $s_{out} = \overline{s_{in}}$

<!--
Модель Эберса-Молла
-->

## Понятие абстракции

Удаления физических, пространственных или временных деталей или атрибутов при изучении объектов или систем для сосредоточения внимания на более важных деталях.

<!--
Общее понятие абстракции.
Примеры?
В программировании?
-->

## Уровни абстракции

![height:10cm center](images/abstraction.png)

> http://waanin.github.io/2016/02/13/inside-cpu-ppt/

## OSI модель

![height:13cm center](images/osi_model.jpeg)

## Электрические интерфейсы

![height:10cm center](images/sockets.jpg)

> https://www.dignited.com/48037/power-outlets-explained-why-there-are-different-plugs-sockets-in-the-world/

## Понятие интерфейса

Граница между двумя функциональными объектами, требования к которой определяются стандартом; совокупность средств, методов и правил взаимодействия (управления, контроля и т. д.) между элементами системы.

## Интерфейсы в программировании

```java
// Interface
interface Animal {
  public void animalSound(); // interface method (does not have a body)
  public void sleep(); // interface method (does not have a body)
}

// Pig "implements" the Animal interface
class Pig implements Animal {
  public void animalSound() {
    // The body of animalSound() is provided here
    System.out.println("The pig says: wee wee");
  }
  public void sleep() {
    // The body of sleep() is provided here
    System.out.println("Zzz");
  }
}
```

## Основные функции процессора

  * Арифметические действия над данными
  * Обращение к памяти
  * Обращение к периферийным устройствам
  * Условные переходы
  * В соответствии с заданным алгоритмом

<!--
Что делает процессор?
-->

## Основные архитектуры

![height:7cm center](images/architectures.png)

> http://waanin.github.io/css/pics/inside_cpu_ppt/

## Свободные архитектуры

 - [OpenRISC](https://openrisc.io/)
 - [RISCV](https://riscv.org/)
     - [NeoRV32](https://github.com/stnolting/neorv32)
     - [VexRISCV](https://github.com/SpinalHDL/VexRiscv)
     - [SERV](https://github.com/olofk/serv)

## Отличие микропроцессора, микроконтроллера, SoC

  - Микропроцессор – вычислительный блок
  * Микроконтроллер – процессор + память + периферия - ОС
      - [Arduino](https://www.arduino.cc/index.php)
      - [ATTiny/ATMega](https://en.wikipedia.org/wiki/AVR_microcontrollers)
      - [PIC](https://en.wikipedia.org/wiki/PIC_microcontrollers)
  * SoC – процессор + память + периферия + ОС
      - [RaspberryPi](https://www.raspberrypi.org/)
      - [OrangePi](http://www.orangepi.org/)
      - [BananaPi](http://www.banana-pi.com/eindex.asp)
      - [ARMbian](https://www.armbian.com/)

<!--
Области применения
-->

## OrangePi R1

![bg right:55% width:100%](images/op_r1.jpg)

  - [NAT router](https://en.wikipedia.org/wiki/Network_address_translation)
  - [DHCP server](https://www.isc.org/dhcp/)
  - [Firewall](https://netfilter.org/)
  - [VPN server](https://openvpn.net/)
  - [CalDAV & CardDAV server](https://radicale.org/v3.html)

## Роль операционной системы

![bg right:40% auto](images/kernel.png)

  - Виртуализация ресурсов
      - Процессор
      - Память
      - Периферия
  - Организация параллельных вычислений
  - Сохранение данных
  - Предоставление пользовательского интерфейса

## Типы операционных систем

  - Общего назначения
  - Реального времени
  - Мобильные

# Приложение

Передача веб страниц: уровни передачи данных

## Среда передачи данных

| ![height:10cm](images/rj45.jpeg) | ![height:10cm](images/fm_psd_labeled.svg) |
|:-:|:-:|
| 8P8C (RJ45) | Спектр FM радио |

> https://ru.wikipedia.org/wiki/8P8C
> https://pysdr.org/

## Дифференциальный сигнал

![height:10cm center](images/DiffSignaling.png)

> https://ru.wikipedia.org/wiki/Дифференциальный_сигнал

## Восстановление тактового сигнала

![width:12cm center](images/ClockRecovery.png)

> https://zitoc.com/digital-modulation/

## Передача сигнала

![width:12cm center](images/eye_diagram.jpg)

> https://www.tek.com/

## Кадр Ethernet

![width:25cm center](images/EthernetFrame.png)

> https://en.wikipedia.org/wiki/Ethernet_frame

## Кадр IPv4

![height:8cm center](images/ipv4.png)

> https://intronetworks.cs.luc.edu/

## Протокол TCP

![height:12cm center](images/tcp.png)

> https://intronetworks.cs.luc.edu/

## Протокол HTTP

* Протокол передачи данных (изначально текста)
* Определены методы (GET, POST, DELETE и т.д.)
* Определены коды состояний (200, 404, 301 и т.д.)
* Шифрование TSL (SSL): HTTPS

## HTML

```html
<!DOCTYPE html>
<html lang="ru">
   <head>
      <meta charset="UTF-8">
      <title>HTML Document</title>
   </head>
   <body>
      <p>
         <b>
            Этот текст будет полужирным, <i>а этот — ещё и курсивным</i>.
         </b>
      </p>
   </body>
</html>
```

## CSS

```css
p.note > b {
    color:green;
}
.div {
    border: 1px solid red;
    padding-left: 20px;
}
.title {
    font-size: 20px;
    background-color: red;
}
```

## JavaScript

```javascript
window.onload = () => {
    const linkWithAlert = document.getElementById('alertLink');
    linkWithAlert.addEventListener('click', async () => {
      if (confirm('Вы уверены?')) {
        await fetch('delete', {method: 'DELETE'})
      }
    })
};
```

## API

* Адреса доступа для программного сбора/обновления информации
* Используется в динамически генерируемых сайтах
* Популярный подход: REST API
* Используется для предоставления данных сторонним приложениям
   ```bash
    curl https://api.open-meteo.com/v1/forecast\?latitude\=55.45\&longitude\=37.37\&current\=temperature_2m | jq
   ```

---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Представление данных

Микропроцессорные системы

## Порядок байтов (Endianness)

- От старшего к младшему (Bin enidian)
* От младшего к старшему (Little enidian)

## От старшего к младшему

![bg right width:100%](./images/Big-Endian.svg)

```
Число
||01|23|45|67|89|ab|cd|ef||
Предстваление в памяти
||01|23|45|67|89|ab|cd|ef||
```

- Используется в большом количестве протоколов передачи данных
* Также называется сетевым порядком
* Многие процессоры могут переключаться на этот порядок байтов

## От младшего к старшему

![bg right width:100%](./images/Little-Endian.svg)

```
Число
||01|23|45|67|89|ab|cd|ef||
Предстваление в памяти
||ef|cd|ab|89|67|45|23|01||
```

- Используется в большинстве процессоров
* Популярность обусловлена успехом архитектуры x86

## Порядок байтов в программировании

- `host` -- последовательность байтов на ЭВМ
- `network` -- сетевая последовательность байтов (от старшего к младшему)
- `T hton*(T)` -- перевод в сетевую последовательность байтов
- `T ntoh*(T)` -- перевод в последовательность байтов ЭВМ
- множество других функций манипуляции последовательностью байтов, не входящих в стандарт POSIX

Следует использовать эти функции, чтобы программа работала на ЭВМ с любой последовательностью байтов

> https://en.wikipedia.org/wiki/Endianness
> https://en.wikipedia.org/wiki/POSIX

## Кодировка ASCII

**A**merican **S**tandard **C**ode for **I**nformation **I**nterchange

- Кодировка латинского алфавита, знаков препинания и специальных символов
- Кодируется 7ю битами
- Повсеместно используется как основа для 8ми-битных кодировок
- Например, `a = 0x61`, `A = 0x41`, `0 = 0x30`
* `bytes([b - (0x61-0x41) for b in b'hello'])`

Конвертация в Python
```python
chr(number) -> char
ord(char) -> number
```

## 8ми-битные кодировки

- Базируются на ASCII
* Помимо базового латинского алфавита, содержат дополнительный алфавит или набор символов (KOI-8, ISO-8859-5 и т.д.)
* Являются неиссякаемым источником проблем с кодировками файлов и шрифтами

## Unicode

- Кодируется различным количеством байтов (1 или 2 байта для UTF-8)
* Совместим с ASCII
* Занимает больше места, чем 8ми-битные кодировки
* Решает большинство проблем 8ми-битных кодировок
* Используется большинством Web ресурсов
* Рекомендуется к использованию вместо 8ми-битных кодировок

## JSON

**J**ava**S**cript **O**bject **N**otation

- Представление типизированных данных в текстовом виде
* Повсеместно используется в Web

```json
{
	"object": {"key": "value"},
	"array": [1, 2, 3],
	"string": "value",
	"number": 1.23,
	"bool": false,
	"null": null
}
```

## YAML

**Y**AML **A**in't **M**arkup **L**anguage

- Представление типизированных данных в текстовом виде
* Проще для восприятия человеком, чем JSON

```yaml
#Comment
root:
  object:
    key: value
  array:
    - 1
    - 2
    - 3
  string: value
  number: 1.23
```

## CBOR

**C**oncise **B**inary **O**bject **R**epresentation

- Представление типизированных данных в бинарном виде
* Попытка сделать "бинарный JSON" формат
* Один из многих конкурирующих стандартов

## Схема данных

Для форматов данных типа JSON и YAML существует проблема соответствия полей объекта определённой структуре.
Для их проверки используются файлы спецификации полей, называющиеся схемами данных (по аналогии с базами данных).

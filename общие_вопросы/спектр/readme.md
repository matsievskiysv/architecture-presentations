---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Сигналы в частотной и временной областях

Микропроцессорные системы

## Частотная и временная область

![height:10cm center](./images/time_and_freq_domain_example_signals.png)

> [https://pysdr.org/](https://pysdr.org/)

## Ряды Фурье

![height:10cm center](./images/fourier_series_arbitrary_function.gif)

> [https://pysdr.org/](https://pysdr.org/)

## Ряды Фурье

| ![height:4cm center](images/dc-signal.png) | ![height:4cm center](images/impulse.png) |
|:-:|:-:|
| DC | Импульс |
| ![height:4cm center](images/sine-wave.png) | ![height:4cm center](images/square-wave.svg) |
| Синус | Меандр |

> [https://pysdr.org/](https://pysdr.org/)

## Преобразование Фурье

Прямое преобразование
$$X(f)=\int{x(t)\operatorname{e}^{-j 2 \pi f t} \operatorname{dt}}$$
Обратное преобразование
$$x(t)=\int{X(f)\operatorname{e}^{j 2 \pi f t} \operatorname{df}}$$
Прямое преобразование дискретного сигнала
$$X_k=\sum_{n=0}^{N-1}{x_n\operatorname{e}^{-j \frac{2 \pi}{N}kn} \operatorname{dt}}$$

> [https://pysdr.org/](https://pysdr.org/)

## Свойства преобразования Фурье

- Линейность:
  $$a x(t)+b y(t) \leftrightarrow a X(f) + b Y(f)$$
- Сдвиг частоты:
  $$\operatorname{e}^{j 2 \pi f_0 t} x(t) \leftrightarrow X(f-f_0)$$
- Масштабирование:
  $$x(a t) \leftrightarrow X(\frac{f}{a})$$
- Умножение частот:
  $$\int{x(\tau)y(t-\tau)\operatorname{d\tau}} \leftrightarrow X(f) Y(f)$$

## Цифровые фильтры

![height:10cm center](./images/windows.svg)

> [https://pysdr.org/](https://pysdr.org/)

## Представление спектра

![height:10cm center](./images/FM.png)

> [https://en.wikipedia.org/wiki/Waterfall_plot](https://en.wikipedia.org/wiki/Waterfall_plot)

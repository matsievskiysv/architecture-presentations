---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Основные элементы процессора

Микропроцессорные системы

## Общая схема процессора

![center height:12cm](./images/scheme.png)

> http://www.edwardbosworth.com/CPSC5155/PH4/Ch04/Ch04_Piplelining.pdf

## Декодер

- Преобразует машинное слово в команду
* Определяет используемые регистры
* Конвертирует численные константы

## Набор инструкций

| Код  | Функции  |
|:--|:--|
| I  | Базовые целочисленные операции  |
| M  | Умножение  |
| A  | Атомарные операции  |
| F/D  | Операции с плавающей точкой  |
| C  | Сжатые операции  |

> RISC-V Reference. James Zhu

## Кодировка инструкций процессора

![bg right width:100%](./images/opcodes.png)

- `opcode`, `funct3`, `funct7` – код команды
- `rd` – регистр назначения
- `rs1`, `rs2` – регистры аргументов
- `imm` – числа

> RISC-V Reference. James Zhu

## CISC и RISC

- Различаются количеством инструкций
* Сложные инструкции требуют специальной поддержки компилятора
* Индивидуальные инструкции RISC оптимизированы

## Регистры

![center height:10cm](./images/registers.png)

> RISC-V Reference. James Zhu

## Zero

```assembly
li rd, <num>
addi rd, zero, <num>

nop
addi zero, zero, 0

j <offset>
jal zero, <offset>
```

## Адрес возврата (RA)

```assembly
jal <offset>
jal ra, <offset>

jalr rs
jalr ra, rs, 0
```

## Счётчик программы (PC)

- Указывает на следующую исполняемую инструкцию
* В разных архитектурах может быть доступен или недоступен

## Кадр стека

![center height:10cm](./images/stackframe.png)

> https://stffrdhrn.github.io/software/embedded/openrisc/2018/06/08/gcc_stack_frames.html

## АЛУ

- Производит операции над данными
* Выполнен на основе мультиплексора

## Контроллер памяти

- Исполняет операции загрузки/записи данных в оперативную память
* Использует кэш для ускорения обращений
  * При отсутствии данных в кэше (промахе) данные загружаются из памяти
  * Используются раздельные кэши для инструкций и данных

## FPU

- Операции с плавающей точкой выполняет FPU
* Имеет свои регистры
* Операции выполняются дольше, чем целочисленные

## Шина процессора

![bg right height:10cm](./images/wishbone.png)

- [Wishbone](https://en.wikipedia.org/wiki/Wishbone_(computer_bus))
- [Simple Bus Architecture](https://en.wikipedia.org/wiki/Simple_Bus_Architecture)

## Устройства ввода/вывода

![center height:10cm](./images/clock_register.png)

## Прерывания

- По внешнему или внутреннему событию процессор может прервать выполняемую программу
* Прерывания обрабатывает обработчик прерываний
* Контекст выполняемой операции должен быть сохранён на время обработки прерывания
* Прерывания разделяют по приоритетам
* После обработки прерывания контекст восстанавливается, и программа возобновляет работу

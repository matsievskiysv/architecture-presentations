library ics;
use ics.ics.all;

entity alu is
  generic (t_rise, t_fall: time := 5 ns;
           bus_size: integer := 8);
  port (clk: in boolean;
        op: in opcode;
        a, b: in integer range 0 to 2**bus_size-1;
        o: out integer range 0 to 2**bus_size-1);
end;

architecture behavioral of alu is
begin
  do_math: process
  begin
    wait until clk and clk'event;
    wait for t_rise;
    case op is
      when ics_add =>
        o <= a + b;
      when ics_sub =>
        o <= a - b;
      when others =>
        o <= 0;
    end case;
    wait for t_fall;
  end process;
end;

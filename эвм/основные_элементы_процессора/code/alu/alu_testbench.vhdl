library ics;
use ics.ics.all;

entity alu_testbench is
end;

architecture rtl of alu_testbench is
  component alu is
    generic (t_rise, t_fall: time := 5 ns;
             bus_size: integer := 8);
    port (clk: in boolean;
          op: in opcode;
          a, b: in integer range 0 to 2**bus_size;
          o: out integer range 0 to 2**bus_size);
  end component;
  constant delay: time := 1 us;
  constant tick: time := 100 ns;
  signal clk: boolean := false;
  signal op: opcode;
  signal a, b, c: integer range 0 to (2**bus_size)-1;
begin
  dut : alu
    generic map (bus_size => BUS_SIZE, t_rise  => 5 ns, t_fall => 5 ns)
    port map(
      clk => clk,
      op => op,
      a => a,
      b => b,
      o => c);
  clk <= not clk after tick;
  process is
  begin
    op <= ics_add;
    a <= 5;
    b <= 3;
    wait for delay;
    assert c = 8 report "5 + 3 != 8" severity failure;
    op <= ics_sub;
    a <= 5;
    b <= 3;
    wait for delay;
    assert c = 2 report "5 - 3 != 2" severity failure;
  end process;
end;

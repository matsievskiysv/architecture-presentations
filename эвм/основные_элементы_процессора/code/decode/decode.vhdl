library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

library ics;
use ics.ics.all;

entity decode is
  generic (t_rise, t_fall: time := 5 ns;
           opcode_size: integer := 4;
           reg_size: integer := 2);
  port (clk: in boolean;
        code: in std_logic_vector(opcode_size+2*reg_size-1 downto 0);
        reg1, reg2: out std_logic_vector(reg_size-1 downto 0);
        op: out opcode);
end;

architecture behavioral of decode is
begin
  decode: process
  begin
    wait until clk and clk'event;
    wait for t_rise;
    reg1 <= code(reg_size+opcode_size-1 downto opcode_size);
    reg2 <= code(2*reg_size+opcode_size-1 downto reg_size+opcode_size);
    op <= ics_convert(code(opcode_size-1 downto 0));
    wait for t_fall;
  end process;
end;

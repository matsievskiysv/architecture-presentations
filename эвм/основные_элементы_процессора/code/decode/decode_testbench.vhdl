library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

library ics;
use ics.ics.all;

entity decode_testbench is
end;

architecture rtl of decode_testbench is
  component decode is
    generic (t_rise, t_fall: time := 5 ns;
             opcode_size: integer := 4;
             reg_size: integer := 2);
    port (clk: in boolean;
          code: in std_logic_vector(opcode_size+2*reg_size-1 downto 0);
          reg1, reg2: out std_logic_vector(reg_size-1 downto 0);
          op: out opcode);
  end component;
  constant delay: time := 1 us;
  constant tick: time := 100 ns;
  signal clk: boolean := true;
  signal code: std_logic_vector(BUS_SIZE-1 downto 0);
  signal reg1, reg2: std_logic_vector(REG_SIZE-1 downto 0);
  signal op: opcode;
begin
  dut : decode
    generic map (opcode_size => CODE_SIZE, reg_size => REG_SIZE, t_rise  => 5 ns, t_fall => 5 ns)
    port map(
      clk => clk,
      code => code,
      reg1 => reg1,
      reg2 => reg2,
      op => op);
  clk <= not clk after tick;
  process is
  begin
    code <= "10101001";
    wait for delay;
    assert (op = ics_add) report "decode error" severity failure;
    code <= "10001110";
    wait for delay;
    assert (op = ics_sub) report "decode error" severity failure;
    code <= "10001111";
    wait for delay;
    assert (op = ics_err) report "decode error" severity failure;
  end process;
end;

library ieee;
use ieee.std_logic_1164.all;

package ics is
  constant BUS_SIZE: integer := 8;
  constant CODE_SIZE: integer := 2;
  constant REG_SIZE: integer := 3;
  type opcode is (ics_err, ics_add, ics_sub);
  attribute enum_encoding : string;
  attribute enum_encoding of opcode : type is "00 01 10";

  pure function ics_convert (input : std_logic_vector(CODE_SIZE-1 downto 0)) return opcode;

end package ics;


package body ics is

  pure function ics_convert (input : std_logic_vector(CODE_SIZE-1 downto 0)) return opcode is
  begin
    case input is
      when "01" =>
        return ics_add;
      when "10" =>
        return ics_sub;
      when others =>
        return ics_err;
    end case;
  end function;

end package body ics;

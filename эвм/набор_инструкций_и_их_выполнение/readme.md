---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Набор инструкций и их выполнение

Микропроцессорные системы

## Группы инструкций

- Base Integer Instructions (I)
* Multiply Extension (M)
* Atomic Extension (A)
* Floating-Point Extensions (D/F)
* Compressed Extension (C)

## Base Integer Instructions: addi/li/mv/nop

```
|31     20|19 15|14  12|11 7|6    0|
|imm[11:0]|  rs1|funct3|  rd|opcode| I-type
```

```
addi a0, zero, 3
li a1, 2
mv a2, a1
nop
```

## Base Integer Instructions: add/sub

```
|31  25|24 20|19 15|14  12|11 7|6    0|
|funct7|  rs2|  rs1|funct3|  rd|opcode| R-type
```

```
li a0, 3
li a1, 2

add a2, a0, a1
sub a3, a0, a1
```

## Base Integer Instructions: xor/or/and/not

```
|31  25|24 20|19 15|14  12|11 7|6    0|
|funct7|  rs2|  rs1|funct3|  rd|opcode| R-type
```

```
li a0, 0b1011001
li a1, 0b1110010

xor a2, a0, a1
or a3, a0, a1
and a4, a0, a1
not a5, a0
neg a6, a0
```

## Base Integer Instructions: slli/srli/srai

```
|31     20|19 15|14  12|11 7|6    0|
|imm[11:0]|  rs1|funct3|  rd|opcode| I-type
```

```
li a0, 0b100

slli a1, a0, 1
srli a2, a0, 1

li a3, -3

srli a4, a3, 1
srai a5, a3, 1
```

## Base Integer Instructions: lw/sw

```
|31     20|19 15|14  12|11 7|6    0|
|imm[11:0]|  rs1|funct3|  rd|opcode| I-type
```

```
|31     25|24 20|19 15|14  12|11     7|6    0|
|imm[11:5]|  rs2|  rs1|funct3|imm[4:0]|opcode| S-type
```

```
.bss
x: .word 0xdeadbeef
y: .word 0

.text
la a0, x
la a1, y

lw a2, 0(a0)
sw a2, 0(a1)
```

## Base Integer Instructions: b\<comp\>

```
|31        25|24 20|19 15|14  12|11        7|6    0|
|imm[12|10:5]|  rs2|  rs1|funct3|imm[4:1|11]|opcode| B-type

|31                 12|11 7|6    0|
|imm[20|10:1|11|19:12]|  rd|opcode| J-type

li a0, 1
li a1, 1

beq a0, a1, br_eq
nop
bne a0, a1, br_neq
nop
br_eq:
li a2, 1
j exit
nop
br_neq:
li a2, 2
j exit
nop
exit:
nop
```

## Multiply Extension: mul/div/rem

```
|31  25|24 20|19 15|14  12|11 7|6    0|
|funct7|  rs2|  rs1|funct3|  rd|opcode| R-type
```

```
li a0, 5
li a1, 2

mul a2, a0, a1
div a3, a0, a1
rem a4, a0, a1
```

## Выполнение инструкций. Конвейер

![center height:12cm](./images/parallelism.png)

> http://www.edwardbosworth.com/CPSC5155/PH4/Ch04/Ch04_Piplelining.pdf

## Этапы конвейера

- Instruction fetch (IF)
- Instruction decode & register read (ID)
- Execute (EX)
- Access memory (MEM)
- Write back (WB)

## Instruction fetch

![center height:12cm](./images/if.png)

> http://www.edwardbosworth.com/CPSC5155/PH4/Ch04/Ch04_Piplelining.pdf

## Instruction decode

![center height:12cm](./images/id.png)

> http://www.edwardbosworth.com/CPSC5155/PH4/Ch04/Ch04_Piplelining.pdf

## Execute

![center height:12cm](./images/ex.png)

> http://www.edwardbosworth.com/CPSC5155/PH4/Ch04/Ch04_Piplelining.pdf

## Access memory

![center height:12cm](./images/mem.png)

> http://www.edwardbosworth.com/CPSC5155/PH4/Ch04/Ch04_Piplelining.pdf

## Write back

![center height:12cm](./images/wb.png)

> http://www.edwardbosworth.com/CPSC5155/PH4/Ch04/Ch04_Piplelining.pdf

## Конфликты конвейера

Невозможность выполнить команду
- Structure hazard: ресурс занят
- Data hazard: аргумент операции ещё не доступен
- Control hazard: операция зависит от выполнения условной операции

## Задержки конвейера

![center height:6cm](./images/bubble.png)

> http://www.edwardbosworth.com/CPSC5155/PH4/Ch04/Ch04_Piplelining.pdf

## Выполнение вне очереди

![center height:6cm](./images/reorder.png)

> http://www.edwardbosworth.com/CPSC5155/PH4/Ch04/Ch04_Piplelining.pdf

## Предсказание ветвления

![center height:12cm](./images/prediction.png)

> http://www.edwardbosworth.com/CPSC5155/PH4/Ch04/Ch04_Piplelining.pdf

## Spectre

![center height:6cm](./images/spectre.png)

> https://en.wikipedia.org/wiki/Spectre_(security_vulnerability)

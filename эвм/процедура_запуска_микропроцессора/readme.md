---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Сборка и выполнение программ

Микропроцессорные системы

## Вектор прерываний

![bg right width:100%](./images/interrupt-vector-table.jpg)

- Определённые архитектурой адреса
* Содержат ссылки на обработчики событий
* Содержат точку входа программы
* Иногда содержат дополнительную информацию (начальное значение регистра `sp` в ARM)

## Этапы загрузки

- Запуск загрузчика (bootloader)
  * Подготовка системы к загрузке операционной системы в память
  * Возможно наличие загрузчиков различных уровней
- Запуск операционной системы
  * Инициализация устройств, не настроенных загрузчиком
  * Загрузка пользовательской программы

## Программирование загрузчика

![bg right:40% width:100%](./images/chipprog.jpg)

- Первоначальный загрузчик должен находиться в энергонезависимой памяти с возможностью чтения отдельных машинных слов
  * Запись при помощи программатора в NOR
  * Использование встроенного загрузчика с поддержкой устройств NAND

> https://www.phyton.ru/

## Загрузка STM32F101

- Адрес `0x0` содержит начальное значение регистра `sp`
* Адрес `0x4` содержит адрес первой исполняемой инструкции
* Перенос секции `.data` из ПЗУ в ОЗУ
* Включение генераторов тактовых импульсов для требуемых блоков
* Настройка периферии

## Подготовка использования C

```c
void foo(void) {
	register int a = 0;
}
```

```assembly
0800016c <foo>:
 800016c:       b480            push    {r7}
 800016e:       af00            add     r7, sp, #0
 8000170:       bf00            nop
 8000172:       46bd            mov     sp, r7
 8000174:       bc80            pop     {r7}
 8000176:       4770            bx      lr
```

## Этапы загрузки Das U-Boot

- Начальная инициализация процессора `start.S`
  * Без ОЗУ, используются только регистры
* `board_init_f()` настройка SRAM, DRAM и UART
  * С ОЗУ, без `BSS`
* `board_init_r()` настройка устройств, из которых будет загружена ОС. подготовка запуска ОС
* Загрузка ОС

## Драйверы устройств

![bg right width:100%](./images/driver.png)

- Абстракция над реализацией устройства
* Предоставляет интерфейс работы с устройством
* Могут образовывать иерархии
* Могут иметь механизм детектирования устройства

## DeviceTree

```dts
/dts-v1/;

/ {
    node1 {
        a-string-property = "A string";
        a-string-list-property = "first string", "second string";
        // hex is implied in byte arrays. no '0x' prefix is required
        a-byte-data-property = [01 23 34 56];
        child-node1 {
            first-child-property;
            second-child-property = <1>;
            a-string-property = "Hello, world";
        };
        child-node2 {
        };
    };
    node2 {
        an-empty-property;
        a-cell-property = <1 2 3 4>; /* each number (cell) is a uint32 */
        child-node1 {
        };
    };
};
```

## Загрузка ОС

- Подготовка разархивирования ОС, разархивирование
* Включение страниц памяти
* Настройка прерываний
* Монтирование `init.rd`
* Загрузка драйверов устройств
* Подключение дисков, создание `root.fs`, `pivot_root()`
* Загрузка пользовательского процесса `PID 1`

## Системные вызовы

- Обращение от пользовательской программы к ОС
* Выделение памяти
* Запрос на работу с устройствами, файлами, передачу данных

## Сигналы

- Асинхронные стандартизованные сообщения программе от ОС и других программ
  - `SIGHUP`
  - `SIGKILL`
  - `SIGTERM`
  - `SIGSTOP`
  - `SIGCONT`


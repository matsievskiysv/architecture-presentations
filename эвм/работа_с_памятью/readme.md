---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Работа с памятью

Микропроцессорные системы

## Классификация памяти

- Энергозависимая/энергонезависимая (volatile/non-volatile)
* Перезаписываемая/неперезаписываемая
* Адресное/блочное чтение/запись

## SRAM

Статическая память с произвольным доступом (static random access memory)

- Быстрый доступ
* Простая схемотехника
* Не требует контроллера
* Возможны очень низкие частоты синхронизации
* Невысокая плотность записи

## DRAM

Динамическая память с произвольным доступом (dynamic random access memory)

- Быстрый доступ
* Сложная схемотехника
* Требует контроллера
* Высокая плотность записи

## Инициализация DRAM

- Включение питания
* Первоначальная настройка регистров
* Калибровка сопротивления (ZQ) / референтного напряжения (Vref DQ)

> https://www.systemverilog.io/ddr4-initialization-and-calibration

## Тренировка DRAM

![bg right width:100%](./images/eye.webp)

- Выравнивание синхроимпульса с данными
* Определение задержки чтения/записи
* Выравнивание диаграммы передачи данных
* Определение ошибок

> https://www.systemverilog.io/ddr4-initialization-and-calibration

## Калибровка DRAM

- Периодическая подстройка сопротивления (ZQ)
* Периодическое выравнивание диаграммы передачи данных

> https://www.systemverilog.io/ddr4-initialization-and-calibration

## Использование SRAM и DRAM

- DRAM
  * Основная оперативная память
- SRAM
  * Регистры
  * Кэш
  * Основная оперативная память во встраиваемых устройствах

## Технологии ПЗУ

- ROM — программируется при изготовлении
* PROM — программируется единожды
* EPROM — стирается ультрафиолетом
* EEPROM — стирается напряжением

## Flash память

Как EEPROM, но стирать данные можно только блоками

- NOR
  * Двухмерная матрица
  * Меньшая плотность данных
  * Адресация к индивидуальным ячейкам
- NAND
  * Трёхмерная матрица
  * Большая плотность данных
  * Адресация к блокам ячеек

## Применение Flash памяти

- NOR
  * Возможна адресация к индивидуальным ячейкам, можно выполнять команды напрямую
  * Используется для загрузчиков (bootloader)
- NAND
  * Незаможна адресация к индивидуальным ячейкам. Для выполнения команд надо сначала переместить их в ОЗУ
  * Используется для сохранения данных
- ONENAND
  * NAND с буфером для индивидуальной адресации

## Другие запоминающие устройства

- Магнитные
  * Дискеты
  * Жёсткие магнитные диски
  * Магнитные ленты
- Оптические
  * CD
  * DVD
  * Blue-Ray

## Архитектура работы с памятью

![bg right:40% width:100%](./images/Von-Neuman-Vs-Harvard-Architecture.png)

- Фон Неймана
- Гарвардская
- Модифицированная Гарвардская

## Скорости обращения к памяти

- Регистры – ~1 машинный цикл
- L1 кэш – 0.8 нс
- L2 кэш – 2.4 нс
- L3 кэш – 11.1 нс
- ОЗУ – 45.5 нс
- SSD – 50 мкс
- HDD – 10 мс

## Кеширование данных

![center width:20cm](./images/cache.png)

## Буферизация

![center width:20cm](./images/buffer.png)

## Прямой доступ к памяти

- Direct memory access (DMA)
- Управляется DMA контроллером

![bg right width:100%](./images/dma.webp)

## Адресное пространство

![bg right width:100%](./images/memory_map.png)

- Адреса разделены на области
* Доступ при помощи операций доступа к памяти процессора
* Работа с устройствами вне адресного пространства осуществляется через промежуточные контроллеры

## Проблема многозадачности

![bg right height:80%](./images/multitasking_memory.png)

- Программы расположены в определённых областях
* Для перемещения требуется переписывать адреса

## Виртуальная память

![bg right width:100%](./images/virt_mem_map.jpeg)

- Реальная память делится на сегменты (страницы)
* Процессы используют виртуальные адреса
* ОС преобразует виртуальные адреса в реальные с использованием таблиц TLB

## Страницы памяти

- Поддержка многозадачности
* Перемещение программ в ОЗУ
* Выгрузка неактивных программ на жёсткий диск
* Защита памяти при помощи атрибутов страниц
* Общие страницы
* Требуется хранить таблицы памяти
* Промахи (TLB miss) снижают производительность

## Секции программы

![bg right width:100%](./images/memory_map_virt.png)

```c
int x;           // BSS
int y = 1;       // DATA
const int z = 2; // RODATA

int foo(int arg1, int arg2) {     // STACK
    int a = 0;                    // STACK
    int *b = malloc(sizeof(int)); // HEAP
    ...
    free(b);
}
```

## Стек

![bg right height:80%](./images/stack.png)

```c
int mult(int x) {
    int a = 2, b = 3;
    return 2 * x + b;
}

void foo(void) {
    int a = mult(mult(mult(mutl(2))));
}
```

```c
int factorial(int n) {
  if (n <= 1)
    return 1;
  else
    return factorial(n - 1);
}

int main(void) {
    return factorial(10);
}
```

## Куча

- Для больших данных, которые не помещаются в стек
* Для сохраняемых между вызовами данных
* Выделение/возвращение памяти командами `malloc`/`free`
* Подвержена сегментации
* Существуют разные алгоритмы выделения памяти (allocators)

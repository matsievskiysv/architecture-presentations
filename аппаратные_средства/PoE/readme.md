---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Power over Ethernet

Микропроцессорные системы

## Power over Ethernet

- Питание сетевых устройств по сигнальному кабелю
- Упрощает расположение устройств
- Часто используется для питания камер наблюдения и IP-телефонов

## Классы PoE

| Класс  | Стандарт  | Мощность, [W]  | Пары проводов  |
|:-:|:-:|:-:|:-:|
| 0  | 802.3af  | 15,4  | 2 |
| 1  | 802.3af  | 4  | 2 |
| 2  | 802.3af  | 7  | 2 |
| 3  | 802.3af  | 15,4  | 2 |
| 4  | 802.3af  | 30  | 2 |
| 5  | 802.3at  | 45  | 4 |
| 6  | 802.3at  | 60  | 4 |
| 7  | 802.3bt  | 75  | 4 |
| 8  | 802.3bt  | 90  | 4 |


## Схема работы

![height:12cm center](./images/poe_schematic.jpg)

> [https://www.poepower.net/](https://www.poepower.net/)

## Этапы работы

- Детектирование
- Классификация
- Работа
- Отключение

![width:100% bg right](./images/startup.png)

## Детектирование

![height:10cm bg right](./images/detection.jpg)

| Параметр  | Min  | Max  |
|:-:|:-:|:-:|
| $R_\text{det}$, кОм  | 23,7  | 26,3  |
| $V_\text{offset}$, В  | 0  | 1,9  |
| $C_\text{in}$, мкФ  | 0,05  | 0,12  |

$V = [2,7-10,1] \text{В}$

> [https://www.poepower.net/](https://www.poepower.net/)

## Классификация I

![width:100% bg right](./images/classification.png)

| Ток, мА  | Класс  |
|:-:|:-:|
| [0-5] | 0 |
| [8-13] | 1 |
| [16-21] | 2 |
| [25-31] | 3 |
| [35-45] | 4 |

> [https://www.poepower.net/](https://www.poepower.net/)

## Классификация II

| Класс  | Событие 1 | Событие 2 | Событие 3 | Событие 4 | Событие 5 |
|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | 1 | 1 | 1 | — | — |
| 2 | 2 | 2 | 2 | — | — |
| 3 | 3 | 3 | 3 | — | — |
| 4 | 4 | 4 | 4 | — | — |
| 5 | 4 | 4 | 0 | 0 | — |
| 6 | 4 | 4 | 1 | 1 | — |
| 7 | 4 | 4 | 2 | 2 | 2 |
| 8 | 4 | 4 | 3 | 3 | 3 |

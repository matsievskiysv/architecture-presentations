UNIT := shift_register_8bit_testbench
WAVE := $(UNIT).ghw
SYM_TIME ?= 500us

VHDL_FILES := $(wildcard *.vhdl)

.DEFAULT_GOAL = run

PHONY: import
import:
	ghdl import $(VHDL_FILES)

PHONY: check
check: import
	ghdl -s $(VHDL_FILES)

PHONY: analyze
analyze: import
	ghdl make $(UNIT)

PHONY: run
run: analyze
	ghdl run $(UNIT) --wave=$(WAVE) --stop-time=$(SYM_TIME)

$(WAVE): run

PHONY: view
view: $(WAVE)
	gtkwave $^

PHONY: clean
clean:
	ghdl --remove
	rm -f $(WAVE)
	rm -f $(UNIT).zip

PHONY: zip
zip:
	zip --symlinks --recurse-paths $(UNIT).zip .

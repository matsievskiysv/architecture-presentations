library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity shift_register_8bit is
  port (D, CLK	: in std_logic;
        Q	: out std_logic_vector(7 downto 0) := (others => 'Z'));
end;

architecture shift_register_8bit of shift_register_8bit is
  component shift_register_4bit is
    port (D, CLK: in std_logic;
          Q	: out std_logic_vector(3 downto 0) := (others => 'Z'));
  end component;
  signal iq1d: std_logic;
begin
  D1 : shift_register_4bit
    port map(
      D => D,
      CLK => CLK,
      Q(2 downto 0) => Q(2 downto 0),
      Q(3) => iq1d);
  D2 : shift_register_4bit
    port map(
      D => iq1d,
      CLK => CLK,
      Q => Q(7 downto 4));
  Q(3) <= iq1d;
end;

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity shift_register_4bit_testbench is
end;

architecture rtl of shift_register_4bit_testbench is
  component shift_register_4bit is
    port (D, CLK: in std_logic;
          Q	: out std_logic_vector(3 downto 0) := (others => 'Z'));
  end component;
  signal input	: std_logic_vector(1 downto 0)	:= (others => '0');
  signal output	: std_logic_vector(3 downto 0)	:= (others => '0');
begin
  dut : shift_register_4bit
    port map(
      D => input(0),
      CLK => input(1),
      Q => output);
  input(1) <= not input(1) after 5 us;
  process
  begin
    -- 0b0110111001
    wait for 5 us;
    input(0) <= '0'; wait for 10 us;
    input(0) <= '1'; wait for 10 us;
    input(0) <= '1'; wait for 10 us;
    input(0) <= '0'; wait for 10 us;
    input(0) <= '1'; wait for 10 us;
    input(0) <= '1'; wait for 10 us;
    input(0) <= '1'; wait for 10 us;
    input(0) <= '0'; wait for 10 us;
    input(0) <= '0'; wait for 10 us;
    input(0) <= '1'; wait for 5 us;
  end process;
end;

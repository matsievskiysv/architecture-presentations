library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity shift_register_4bit is
  port (D, CLK	: in std_logic;
        Q	: out std_logic_vector(3 downto 0) := (others => 'Z'));
end;

architecture shift_register_4bit of shift_register_4bit is
  component flipflop is
    port (D, CLK: in std_logic;
          Q, NQ	: out std_logic := 'Z');
  end component;
  signal iq: std_logic_vector(3 downto 0) := (others => 'Z');
begin
  DA : flipflop
    port map(
      D => D,
      CLK => CLK,
      Q => iq(0),
      NQ => open);
  DB : flipflop
    port map(
      D => iq(0),
      CLK => CLK,
      Q => iq(1),
      NQ => open);
  DC : flipflop
    port map(
      D => iq(1),
      CLK => CLK,
      Q => iq(2),
      NQ => open);
  DD : flipflop
    port map(
      D => iq(2),
      CLK => CLK,
      Q => iq(3),
      NQ => open);
  Q <= iq;
end;

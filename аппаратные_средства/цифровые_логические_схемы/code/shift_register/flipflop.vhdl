library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity flipflop is
  port (D, CLK	: in std_logic;
        Q, NQ	: out std_logic := 'Z');
end;

architecture flipflop of flipflop is
  component dlatch is
    port (D, E	: in std_logic;
          Q, NQ	: out std_logic := 'Z');
  end component;
  signal data, gate_not, clock, out1, out2: std_logic;
begin
  master : dlatch
    port map(
      D => D,
      E => CLK,
      Q => data,
      NQ => open);
  slave : dlatch
    port map(
      D => data,
      E => gate_not,
      Q => out1,
      NQ => out2);
  gate_not <= not CLK;
  Q <= out1;
  NQ <= out2;
end;

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity multiplexor_testbench is
end;

architecture rtl of multiplexor_testbench is
  component multiplexor is
    port (I0, I1, A	: in std_logic;
          Q		: out std_logic);
  end component;
  signal input		: std_logic_vector(2 downto 0)	:= (others => '0');
  signal output		: std_logic_vector(0 downto 0)	:= (others => '0');
begin
  dut : multiplexor
    port map(
      I0 => input(0),
      I1 => input(1),
      A => input(2),
      Q => output(0));
  process is
  begin
    for i in 0 to 2**input'length-1 loop
      input <= std_logic_vector(to_unsigned(i, input'length));
      wait for 1 us;
    end loop;
  end process;
end;

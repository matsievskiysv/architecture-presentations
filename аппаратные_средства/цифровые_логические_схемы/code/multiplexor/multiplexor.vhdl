library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity multiplexor is
  port (I0, I1, A	: in std_logic;
        Q		: out std_logic);
end;

architecture multiplexor of multiplexor is
  signal gate0, gate1, gate_not	: std_logic	:= '0';
begin
  gate0 <= I0 nand A;
  gate_not <= not A;
  gate1 <= I1 nand gate_not;
  Q <= gate0 nand gate1;
end;

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity flipflop_testbench is
end;

architecture rtl of flipflop_testbench is
  component flipflop is
    port (D, CLK: in std_logic;
          Q, NQ	: out std_logic := 'Z');
  end component;
  signal input		: std_logic_vector(1 downto 0)	:= (others => '0');
  signal output		: std_logic_vector(1 downto 0)	:= (others => '0');
begin
  dut : flipflop
    port map(
      D => input(0),
      CLK => input(1),
      Q => output(0),
      NQ => output(1));
  input(0) <= not input(0) after 7 us;
  input(1) <= not input(1) after 10 us;
end;

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity dlatch is
  port (D, E	: in std_logic;
        Q, NQ	: out std_logic := 'Z');
end;

architecture dlatch of dlatch is
  signal gate0, gate1, gate_not, out1	: std_logic	:= '0';
  signal out2				: std_logic	:= '1';
begin
  gate_not <= not D;
  gate0 <= D nand E;
  gate1 <= E nand gate_not;
  out1 <= gate0 nand out2 after 1 ns;
  out2 <= gate1 nand out1 after 1 ns;
  Q <= out1;
  NQ <= out2;
end;

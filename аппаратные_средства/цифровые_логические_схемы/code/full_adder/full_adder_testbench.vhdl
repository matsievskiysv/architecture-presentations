library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity full_adder_testbench is
end;

architecture rtl of full_adder_testbench is
  component full_adder is
    port (a, b, cin	: in std_logic;
          sum, cout	: out std_logic);
  end component;
  signal input		: std_logic_vector(2 downto 0)	:= (others => '0');
  signal output		: std_logic_vector(1 downto 0)	:= (others => '0');
begin
  adder : full_adder
    port map(
      a => input(0),
      b => input(1),
      cin => input(2),
      sum => output(0),
      cout => output(1));
  process is
  begin
    for i in 0 to 2**input'length-1 loop
      input <= std_logic_vector(to_unsigned(i, input'length));
      wait for 10 us;
    end loop;
  end process;
end;

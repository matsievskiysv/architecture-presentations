library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity full_adder is
  port (a, b, cin	: in std_logic;
        sum, cout	: out std_logic);
end;

architecture full_adder of full_adder is
  component half_adder is
    port (a, b		: in std_logic;
          sum, cout	: out std_logic);
  end component;
  signal int_sum, int_cout1, int_cout2	: std_logic;
begin
  adder1 : half_adder
    port map(
      a => a,
      b => b,
      sum => int_sum,
      cout => int_cout1);
  adder2 : half_adder
    port map(
      a => cin,
      b => int_sum,
      sum => sum,
      cout => int_cout2);
  cout <= int_cout1 or int_cout2;
end;

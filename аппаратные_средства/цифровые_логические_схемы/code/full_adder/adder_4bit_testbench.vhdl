library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity adder_4bit_testbench is
end;

architecture rtl of adder_4bit_testbench is
  component adder_4bit is
    port (num1, num2	: in std_logic_vector(3 downto 0) := (others => '0');
          cin		: in std_logic;
          sum		: out std_logic_vector(3 downto 0) := (others => 'Z');
          cout		: out std_logic);
  end component;
  signal a		: std_logic_vector(3 downto 0)	:= (others => '0');
  signal b		: std_logic_vector(3 downto 0)	:= (others => '0');
  signal s		: std_logic_vector(3 downto 0)	:= (others => '0');
  signal c		: std_logic			:= '0';
begin
  adder : adder_4bit
    port map(
      num1 => a,
      num2 => b,
      cin => '0',
      sum => s,
      cout => c);
  process is
  begin
    for i in 0 to 2**a'length-1 loop
      for j in 0 to 2**b'length-1 loop
        a <= std_logic_vector(to_unsigned(i, a'length));
        b <= std_logic_vector(to_unsigned(j, b'length));
        wait for 1 us;
      end loop;
    end loop;
  end process;
end;

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity adder_4bit is
  port (num1, num2	: in std_logic_vector(3 downto 0) := (others => '0');
        cin		: in std_logic;
        sum		: out std_logic_vector(3 downto 0) := (others => 'Z');
        cout		: out std_logic);
end;

architecture adder_4bit of adder_4bit is
  component full_adder is
    port (a, b, cin	: in std_logic;
          sum, cout	: out std_logic);
  end component;
  signal int_cout : std_logic_vector(2 downto 0);
begin
  adder1 : full_adder
    port map(
      a => num1(0),
      b => num2(0),
      cin => cin,
      sum => sum(0),
      cout => int_cout(0));
  adder2 : full_adder
    port map(
      a => num1(1),
      b => num2(1),
      cin => int_cout(0),
      sum => sum(1),
      cout => int_cout(1));
  adder3 : full_adder
    port map(
      a => num1(2),
      b => num2(2),
      cin => int_cout(1),
      sum => sum(2),
      cout => int_cout(2));
  adder4 : full_adder
    port map(
      a => num1(3),
      b => num2(3),
      cin => int_cout(2),
      sum => sum(3),
      cout => cout);
end;

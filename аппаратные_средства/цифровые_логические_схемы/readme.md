---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Цифровые логические схемы

Микропроцессорные системы

## Транзисторы

![center height:5cm](./images/transistor.png)

  - $I_E = I_{ES}\left\{\exp{\left(\frac{V_{BE}}{V_T}\right)}-1\right\}$
  - $I_C = \alpha_F I_E$
  - $I_B = \left(1-\alpha_F\right) I_E$

## Полевые транзисторы

![center](./images/Типы_полевых_транзисторов.png)

> https://ru.wikipedia.org/wiki/Полевой_транзистор

<!-- Свойства транзистора?
$\alpha_F  \approx 1$
$I_{ES}  \approx 1^{-15}$
$V_T \approx 0.02$
$I_E(V_{BE}=0)\approx 0$
$I_E(V_{BE}=5)\approx \infty$
-->

## Подтягивающие резисторы

![center height:8cm](./images/Pullup_Resistor.png)

> https://ru.wikipedia.org/wiki/Подтягивающий_резистор

## Не

![center height:10cm](./images/not-gate.png)

> https://www.kicad.org/

<!-- Какая таблица истинности у этого элемента? -->

## И

![center height:12cm](./images/nand-gate.png)

> https://www.kicad.org/

<!--
Какая таблица истинности у этого элемента?
Это И?
-->

## Мультиплексор

![bg right width:100%](./images/multiplexer.png)

| A  | I0  | I1  | Q  |
|:-:|:-:|:-:|:-:|
| 1  | 0  | X  | 0  |
| 1  | 1  | X  | 1  |
| 0  | X  | 0  | 0  |
| 0  | X  | 1  | 1  |

> https://www.kicad.org/

## D-триггер

![bg right width:100%](./images/d-latch.png)

| D  | E  | Q  | NQ  |
|:-:|:-:|:-:|:-:|
| 0  | 1  | 0  | 1  |
| 1  | 1  | 1  | 0  |
| X  | 0  | Qₚ  | NQₚ  |

> https://www.kicad.org/

## Синхроимпульс

![center width:15cm](./images/синхроимпульс.png)

> https://ru.wikipedia.org/wiki/Синхронизация_(передача_сигналов)

## T-триггер

![bg right width:100%](./images/master-slave-flip-flop.png)

| D  | CLK  | Q  | NQ  |
|:-:|:-:|:-:|:-:|
| 0  | ↓  | 0  | 1  |
| 1  | ↓  | 1  | 0  |
| X  | Const  | Qₚ  | NQₚ  |

> https://www.kicad.org/

## Сдвиговый регистр

![center](./images/shift-register-4bit.png)

> https://www.kicad.org/

<!-- Как из этого сделать цепь задержки? -->

## Полусумматор

![bg right height:10cm](./images/half-adder.png)

| A  | B  | S  | C  |
|:-:|:-:|:-:|:-:|
| 0  | 0  | 0  | 0  |
| 0  | 1  | 1  | 0  |
| 1  | 0  | 1  | 0  |
| 1  | 1  | 0  | 1  |

> https://www.kicad.org/

<!--
Почему такое название?
Что оно делает?
Какие функциональные назначения выводов?
Что такое C?
Как из этого сделать полный сумматор?
-->

## Полный сумматор

![bg right:60% width:100%](./images/full-adder.png)

| A  | B  | CIN  | S  | COUT  |
|:-:|:-:|:-:|:-:|:-:|
| 0  | 0  | 0  | 0  | 0  |
| 0  | 0  | 1  | 1  | 0  |
| 0  | 1  | 0  | 1  | 0  |
| 0  | 1  | 1  | 0  | 1  |
| 1  | 0  | 0  | 1  | 0  |
| 1  | 0  | 1  | 0  | 1  |
| 1  | 1  | 0  | 0  | 1  |
| 1  | 1  | 1  | 1  | 1  |

<!--
Почему такое название?
Что оно делает?
Какие функциональные назначения выводов?
Что такое C?
Как из этого сделать полный сумматор?
-->

## 4x-битный сумматор

![center width:100%](./images/4bit-adder.png)

> https://www.kicad.org/

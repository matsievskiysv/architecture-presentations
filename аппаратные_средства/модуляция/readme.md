---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Модуляция сигналов

Микропроцессорные системы

## Дискретизация сигнала

![height:10cm center](./images/Signal_Sampling.svg)

> [https://ru.wikipedia.org/wiki/Дискретизация](https://ru.wikipedia.org/wiki/Дискретизация)

## Теорема Котельникова

| ![height:4cm center](./images/sampling_Fs_0.3.svg)  | ![height:4cm center](./images/sampling_Fs_0.36.svg)  |
|:-:|:-:|
| $F_s=f$  | $F_s=1.2 f$  |
| ![height:4cm center](./images/sampling_Fs_0.45.svg)  | ![height:4cm center](./images/sampling_Fs_0.6.svg)  |
| $F_s=1.5 f$  | $F_s=2 f$  |

> [https://pysdr.org/](https://pysdr.org/)

## Максимальная частота принимаемого сигнала

![height:10cm center](./images/max_freq.svg)

> [https://pysdr.org/](https://pysdr.org/)

## IQ представление сигнала

![height:10cm center](./images/IQ_wave.png)

$$x(t)=I \cos{2\pi f t} + Q \sin{2\pi f t}$$

> [https://pysdr.org/](https://pysdr.org/)

## Комплексное представление

![height:10cm center](./images/complex_plane_1.png)

> [https://pysdr.org/](https://pysdr.org/)

## Несущая частота

![height:4cm center](./images/downconversion.png)

$$A\cdot\cos{\left(2\pi f t + \varphi\right)}$$
$$\sqrt{I^2+Q^2}\cdot\cos{\left(\underbrace{2\pi f t}_\text{несущая} + \arctan{\frac{Q}{I}}\right)}$$

> [https://pysdr.org/](https://pysdr.org/)


## Архитектуры приёмников сигналов

| ![height:4cm center](./images/receiver_arch_direct_sampling.png)  | ![height:4cm center](./images/receiver_arch_zero_if.png)  |
|:-:|:-:|
| Прямая дискретизация | Прямое преобразование  |

| ![height:4cm center](./images/receiver_arch_superheterodyne.png)  |
|:-:|
| Супергетеродин |


> [https://pysdr.org/](https://pysdr.org/)

## Кодирование символами

![height:4cm center](./images/symbols.png)
![height:7cm center](./images/ethernet.svg)

> [https://pysdr.org/](https://pysdr.org/)

## Беспроводная передача сигналов

![height:8cm center](./images/square-wave.svg)

Проблемы:
* Передача низкочастотных сигналов и постоянной составляющей
* Широкий спектр сигнала

> [https://pysdr.org/](https://pysdr.org/)

## Варианты модуляции

* Амплитудная модуляция (ASK)
* Частотная модуляция (FSK)
* Фазовая модуляция (PSK)

## Амплитудная модуляция

![height:10cm center](./images/ASK.svg)

> [https://pysdr.org/](https://pysdr.org/)

## Частотная модуляция

| ![height:6cm center](./images/fsk.svg)  | ![height:6cm center](./images/fsk2.svg)  |
|:-:|:-:|
|   |   |

> [https://pysdr.org/](https://pysdr.org/)

## Фазовая модуляция

![height:6cm center](./images/bpsk.svg)

> [https://pysdr.org/](https://pysdr.org/)

## Созвездия I

![height:10cm center](./images/psk_set.png)

> [https://pysdr.org/](https://pysdr.org/)

## Созвездия II

![height:10cm center](./images/qam.png)

> [https://pysdr.org/](https://pysdr.org/)

## Ограничение количества уровней

![height:10cm center](./images/qpsk_vs_16qam.png)

> [https://pysdr.org/](https://pysdr.org/)

## Адаптивная передача

![height:10cm bg right](./images/adaptive_mcs.svg)
![height:10cm](./images/adaptive_mcs2.svg)

> [https://pysdr.org/](https://pysdr.org/)

## Дифференциальное кодирование

![height:10cm center](./images/differential_coding.svg)

> [https://pysdr.org/](https://pysdr.org/)

## Синхронизация приёмника

![height:10cm center](./images/time-sync-output.svg)

> [https://pysdr.org/](https://pysdr.org/)

## Кодирование проводных коммуникаций

- 5b/6b
- 8b/10b
- 64b/66b

> [https://en.wikipedia.org/wiki/8b/10b_encoding](https://en.wikipedia.org/wiki/8b/10b_encoding)

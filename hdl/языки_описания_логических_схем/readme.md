---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Языки описания логических схем (HDL)

Микропроцессорные системы

## ПЛИС

![center height:10cm](./images/CPLD_vs_FPGA.jpg)

- Verilog
- VHSIC Hardware Description Language (VHDL)

> https://www.circuitstoday.com/understanding-fpga-and-cpld

## Особенности VHDL

- Разделение на entity и architecture
- Параллельное исполнение команд
- Сигналы – параллельный контекст, переменные – последовательный

## std_logic

|`1`| Логическая 1 |
|:--:|:--|
|`0`| Логический 0 |
|`Z`| Высокий импеданс |
|`W`| Слабый сигнал. 0 или 1 |
|`L`| Слабый 0, подтянут вниз |
|`H`| Слабая 1, подтянута вверх |
|`-`| Не важно |
|`U`| Не инициализирован |
|`X`| Неизвестно. Несколько драйверов |

## VHDL Entity

![bg right:40% width:40%](./images/d-latch-entity.png)

```vhdl
entity dlatch is
  port (D, E	: in std_logic;
        Q, NQ	: out std_logic := 'Z');
end;
```

## VHDL Architecture

![bg right:40% width:100%](./images/d-latch-architecture.png)

```vhdl
architecture dlatch of dlatch is
  signal gate0, gate1, gate_not, out1, out2: std_logic	:= '0';
begin
  gate_not <= not D;
  gate0 <= D nand E;
  gate1 <= E nand gate_not;
  out1 <= gate0 nand out2 after 1 ns;
  out2 <= gate1 nand out1 after 1 ns;
  Q <= out1;
  NQ <= out2;
end;
```

## Испытательный стенд

```vhdl
entity multiplexor_testbench is
end;

architecture rtl of multiplexor_testbench is
  component multiplexor is
    port (I0, I1, A	: in std_logic;
          Q		: out std_logic);
  end component;
  signal input		: std_logic_vector(2 downto 0)	:= (others => '0');
  signal output		: std_logic_vector(0 downto 0)	:= (others => '0');
begin
  dut : multiplexor
    port map(
      I0 => input(0),
      I1 => input(1),
      A => input(2),
      Q => output(0));
  process is
  begin
    for i in 0 to 8 loop
      input <= std_logic_vector(to_unsigned(i, input'length));
      wait for 10 ns;
    end loop;
  end process;
end;
```

## Мультиплексор

<div class="twocols">

```vhdl
entity multiplexor is
  port (I0, I1, A	: in std_logic;
        Q		: out std_logic);
end;

architecture multiplexor of multiplexor is
  signal gate0		: std_logic	:= '0';
  signal gate1		: std_logic	:= '0';
  signal gate_not	: std_logic	:= '0';
begin
  gate0 <= I0 nand A;
  gate_not <= not A;
  gate1 <= I1 nand gate_not;
  Q <= gate0 nand gate1;
end;
```

<p class="break"></p>

```vhdl
entity multiplexor is
  port (I0, I1, A	: in std_logic;
        Q		: out std_logic);
end;

architecture multiplexor of multiplexor is
begin
  with A select Q <=
    I1 when '0',
    I0 when others;
end;
```

</div>

## D-триггер

<div class="twocols">

```vhdl
entity dlatch is
  port (D, E	: in std_logic;
        Q, NQ	: out std_logic := 'Z');
end;

architecture dlatch of dlatch is
  signal gate0, gate1, gate_not, out1	: std_logic	:= '0';
  signal out2				: std_logic	:= '1';
begin
  gate_not <= not D;
  gate0 <= D nand E;
  gate1 <= E nand gate_not;
  out1 <= gate0 nand out2 after 1 ns;
  out2 <= gate1 nand out1 after 1 ns;
  Q <= out1;
  NQ <= out2;
end;
```

<p class="break"></p>

```vhdl
entity dlatch is
  port (D, E	: in std_logic;
        Q, NQ	: out std_logic := 'Z');
end;

architecture dlatch of dlatch is
begin
  process (D, E)
  begin
    if E='1' then
      Q <= D;
      NQ <= not D;
    end if;
  end process;
end;
```

</div>

## T-триггер

<div class="twocols">

```vhdl
entity flipflop is
  port (D, CLK	: in std_logic;
        Q, NQ	: out std_logic := 'Z');
end;

architecture flipflop of flipflop is
  component dlatch is
    port (D, E	: in std_logic;
          Q, NQ	: out std_logic := 'Z');
  end component;
  signal data, gate_not, clock, out1, out2: std_logic;
begin
  master : dlatch
    port map(
      D => D,
      E => CLK,
      Q => data,
      NQ => open);
  slave : dlatch
    port map(
      D => data,
      E => gate_not,
      Q => out1,
      NQ => out2);
  gate_not <= not CLK;
  Q <= out1;
  NQ <= out2;
end;
```

<p class="break"></p>

```vhdl
entity flipflop is
  port (D, CLK	: in std_logic;
        Q, NQ	: out std_logic := 'Z');
end;

architecture flipflop of flipflop is
begin
  process (D, CLK)
  begin
    if falling_edge(CLK) then
      Q <= D;
      NQ <= not D;
    end if;
  end process;
end;
```

</div>

## Сдвиговый регистр

<div class="twocols">

```vhdl
entity shift_register_4bit is
  port (D, CLK	: in std_logic;
        Q	: out std_logic_vector(3 downto 0) := (others => 'Z'));
end;

architecture shift_register_4bit of shift_register_4bit is
  component flipflop is
    port (D, CLK: in std_logic;
          Q, NQ	: out std_logic := 'Z');
  end component;
  signal iq: std_logic_vector(3 downto 0) := (others => 'Z');
begin
  DA : flipflop
    port map(
      D => D,
      CLK => CLK,
      Q => iq(0),
      NQ => open);
  DB : flipflop
    port map(
      D => iq(0),
      CLK => CLK,
      Q => iq(1),
      NQ => open);
  DC : flipflop
    port map(
      D => iq(1),
      CLK => CLK,
      Q => iq(2),
      NQ => open);
  DD : flipflop
    port map(
      D => iq(2),
      CLK => CLK,
      Q => iq(3),
      NQ => open);
  Q <= iq;
end;
```

<p class="break"></p>

```vhdl
entity shift_register is
  generic (
    depth	: integer
  );
  port (D, CLK	: in std_logic;
        Q	: out std_logic_vector(depth-1 downto 0) := (others => 'Z'));
end;

architecture shift_register of shift_register is
  signal buf	: std_logic_vector(depth-1 downto 0) :=	(others => '0');
begin
  process (D, CLK)
  begin
    if falling_edge(CLK) then
      buf <= buf(buf'high-1 downto buf'low) & D;
      Q <= buf;
    end if;
  end process;
end;
```

</div>

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity dlatch is
  port (D, E	: in std_logic;
        Q, NQ	: out std_logic := 'Z');
end;

architecture dlatch of dlatch is
begin
  process (D, E)
  begin
    if E='1' then
      Q <= D;
      NQ <= not D;
    end if;
  end process;
end;

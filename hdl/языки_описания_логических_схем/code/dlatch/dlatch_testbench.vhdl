library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity dlatch_testbench is
end;

architecture rtl of dlatch_testbench is
  component dlatch is
    port (D, E	: in std_logic;
          Q, NQ	: out std_logic := 'Z');
  end component;
  signal input	: std_logic_vector(1 downto 0)	:= (others => '0');
  signal output	: std_logic_vector(1 downto 0)	:= (others => '0');
begin
  dut : dlatch
    port map(
      D => input(0),
      E => input(1),
      Q => output(0),
      NQ => output(1));
  process is
  begin
    for i in 0 to 2**input'length-1 loop
      input <= std_logic_vector(to_unsigned(i, input'length));
      wait for 1 us;
    end loop;
  end process;
end;

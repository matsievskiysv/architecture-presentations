library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity flipflop is
  port (D, CLK	: in std_logic;
        Q, NQ	: out std_logic := 'Z');
end;

architecture flipflop of flipflop is
begin
  process (D, CLK)
  begin
    if falling_edge(CLK) then
      Q <= D;
      NQ <= not D;
    end if;
  end process;
end;

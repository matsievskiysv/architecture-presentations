library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity multiplexor is
  port (I0, I1, A	: in std_logic;
        Q		: out std_logic);
end;

architecture multiplexor of multiplexor is
begin
  with A select Q <=
    I1 when '0',
    I0 when others;
end;

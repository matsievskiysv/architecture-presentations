library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity shift_register is
  generic (
    depth	: integer
  );
  port (D, CLK	: in std_logic;
        Q	: out std_logic_vector(depth-1 downto 0) := (others => 'Z'));
end;

architecture shift_register of shift_register is
  signal buf	: std_logic_vector(depth-1 downto 0) :=	(others => '0');
begin
  process (D, CLK)
  begin
    if falling_edge(CLK) then
      buf <= buf(buf'high-1 downto buf'low) & D;
      Q <= buf;
    end if;
  end process;
end;

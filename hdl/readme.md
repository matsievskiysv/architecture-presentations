<!-- % Микропроцессорные системы -->
<!-- pandoc -f markdown -t pdf --pdf-engine=wkhtmltopdf --pdf-engine-opt=--page-size --pdf-engine-opt=a4 --pdf-engine-opt=--minimum-font-size --pdf-engine-opt=20 --pdf-engine-opt=--page-offset --pdf-engine-opt=1 --pdf-engine-opt=-q -s -o readme.pdf readme.md -->

# Языки описания логических схем (HDL)
## Главы

1. [Языки описания логических схем (HDL)](./языки_описания_логических_схем)
	1. ПЛИС
	1. Особенности VHDL
	1. Entity и Architecture
	1. Концепция испытательных стендов (test bench)


## Литература

1. [Языки описания логических схем (HDL)](./языки_описания_логических_схем)
	* [Synario – VHDL Reference Manual](https://www.ics.uci.edu/~jmoorkan/vhdlref/Synario%20VHDL%20Manual.pdf)

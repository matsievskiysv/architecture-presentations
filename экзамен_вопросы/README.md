# Установка программ

Требуется установка программ

```bash
sudo apt install texlive-full gawk
```

# Изменения текста

Основной файл шаблона [`main.tex`](main.tex). Вопросы берутся из файла [`source.tsv`](source.tsv).

# Подготовка вопросов

В файле [`source.tsv`](source.tsv) следует заполнить первые три колонки вопросами и сохранить файл в том-же формате.

# Подпись

Если в папку поместить файл `signature.png`, то его содержимое будет использовано в качестве подписи.

Размер подписи можно регулировать, подстраивая значение множителя (`0.8`) в строке
```latex
\IfFileExists{signature.png}{\includegraphics[width=0.8\textwidth]{signature.png}}{}
```
в файле [`main.tex`](main.tex).

# Генерация билетов

```bash
./generate < source.tsv
```

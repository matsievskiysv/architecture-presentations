---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# I2C & SMBUS

Микропроцессорные системы

## Передача данных по одному проводу

| ![width:15cm center](./images/I2C2_circuit3.jpg)  | ![width:15cm center](./images/I2C2_circuit4.jpg)  |
|:-:|:-:|
|   |   |

$$
\begin{align}
V_H &= V^1_H \  \& \  V^2_H \  \& \  V^3_H \  \& \ldots \\
V_L &= V^1_L \  \| \ V^2_L \ \| \ V^3_L \ \| \ldots
\end{align}
$$

> [https://www.allaboutcircuits.com/](https://www.allaboutcircuits.com/technical-articles/the-i2c-bus-hardware-implementation-details/)

## Ограничение по скорости

| ![height:10cm center](./images/I2C2_circuit5.jpg)  | ![height:10cm center](./images/I2C2_circuit6.jpg)  |
|:-:|:-:|
|   |   |

> [https://www.allaboutcircuits.com/](https://www.allaboutcircuits.com/technical-articles/the-i2c-bus-hardware-implementation-details/)

## I2C & SMBUS

- Шины со многими ведомыми устройствами в пределах одной или смежных плат
- Используют 2 линии
   - Линия данных SDA
   - Тактирующая линия SCL
- Сигналы статуса
   - `start`: начало передачи
   - `stop`: конец передачи
   - `ack`: подтверждение передачи
   - `nack`: ошибка передачи
- Автоматический арбитраж шины

## I2C vs SMBUS

|   | I2C  | SMBUS  |
|:-|:-:|:-:|
| Минимальная скорость, кГц  | –  | 10  |
| Максимальная скорость, кГц  | 100, 400, 1000, 3400  | 100  |
| Адресация  | Статическая  | Статическая + динамическая (SMBUS ARP)  |

## Посылка данных

![width:25cm center](./images/I2C-PROTOCOL.png)

- Адрес чтения `0bxxxxxxx1` (`0xa1`)
- Адрес записи `0bxxxxxxx0` (`0xa0`)
- Адрес (`0xa1 >> 1` == `0xa0 >> 1` == `0x50`)

> [https://fastbitlab.com/](https://fastbitlab.com/stm32-i2c-lecture-3-i2c-protocol-explanation/)

## Условие `start`

| `SDA`  | `SCL`  |
|:-:|:-:|
| `↓`  | `1`  |

![width:100% bg right:70%](./images/start.jpg)

## Условие `stop`

| `SDA`  | `SCL`  |
|:-:|:-:|
| `↑`  | `1`  |

![width:100% bg right:70%](./images/stop.jpg)

## Условие `ack`

| `SDA`  | `SCL`  |
|:-:|:-:|
| `0`  | `↑↓`  |

![width:100% bg right:70%](./images/start.jpg)

## Условие `nack`

| `SDA`  | `SCL`  |
|:-:|:-:|
| `1`  | `↑↓`  |

![width:100% bg right:70%](./images/stop.jpg)

## Арбитраж шины

- Ведущий следит за состоянием шины перед передачей и ждёт сигнала `stop`
* Ведущий следит за состоянием шины в момент передачи
   - Если при передаче `1` `SDA=0`, ведущий прекращает передачу

![width:100% bg right](./images/i2c_arbitration.png)

## I2C задержка тактирующего сигнала

- Подтягивание `SCL` ведомым к `0` для задержки передачи данных
* Поддерживается не всеми устройствами

![width:100% bg right](./images/stretch.jpg)

## Блокировка I2C

- При потере сигнала ведомое устройство может застрять в состоянии посылки `0`
* Согласно правилам арбитража ведущее устройство не может начать передачу данных
* Требуется посылка тактов `SCL` для возврата в рабочий режим

## I2C преобразование напряжения

![height:10cm center](./images/voltage_convertor.png)

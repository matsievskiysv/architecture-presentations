---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# SPI

Микропроцессорные системы

## Шина SPI

- Последовательная шина
- 2 однонаправленных линии данных
   - `MOSI`: от ведущего
   - `MISO`: к ведущему
- 1 тактирующая линия `SCK`
- Переключение ведомых устройств сигналами `nCS`
- Скорость до десятков МБит/с

![bg right width:100%](./images/SPI-Interface.jpg)

> [https://www.corelis.com/](https://www.corelis.com/education/tutorials/spi-tutorial/)

## Запись данных

![height:8cm center](./images/Simple-SPI-Write-Transaction.jpg)

> [https://www.corelis.com/](https://www.corelis.com/education/tutorials/spi-tutorial/)

## Чтение данных

![height:8cm center](./images/Simple-SPI-Read-Transaction.jpg)

> [https://www.corelis.com/](https://www.corelis.com/education/tutorials/spi-tutorial/)

## Полярность сигнала

| Режим  | `CPOL`  | `CPHA`  |
|:-:|:-:|:-:|
| `0`  | `0`  | `0`  |
| `1`  | `0`  | `1`  |
| `2`  | `1`  | `0`  |
| `3`  | `1`  | `1`  |


![bg right width:100%](./images/SPI-bus-timing.jpg)

> [https://www.corelis.com/](https://www.corelis.com/education/tutorials/spi-tutorial/)

## Полудуплексный режим

| ![width:15cm center](./images/SPI-configuration.jpg)  | ![width:15cm center](./images/Quad-IO-SPI-Transaction.jpg)  |
|:-:|:-:|
|   |   |


> [https://www.corelis.com/](https://www.corelis.com/education/tutorials/spi-tutorial/)

## Каскадное соединение

![height:10cm center](./images/spi-interface-multi-daisy.png)

> [https://www.best-microcontroller-projects.com/](https://www.best-microcontroller-projects.com/)

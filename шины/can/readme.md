---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Controller Area Network: CAN

Микропроцессорные системы

## CAN

![width:100% bg right:40%](./images/car.jpg)

- Изначально предназначена для автомобилей
- Шина со многими устройствами
- Все устройства равноправны
- Помехозащищенная шина: дифференциальная передача данных и контрольные суммы
- Поддержка арбитража сообщений
- Приоритизация сообщений
- Разделение на физический и логический уровни

> https://www.csselectronics.com/pages/can-bus-simple-intro-tutorial

## Стандарты физического уровня

| Стандарт  | Скорость  |
|:-:|:-:|
| Single wire CAN  | 33.3 кБ/с  |
| Low speed CAN  | 125 кБ/с  |
| High speed CAN  | 1 МБ/с  |
| CAN FD  | 8 МБ/с  |

![width:100% bg right:50%](./images/interface-can-bus.png)

> [circuitbread](https://www.circuitbread.com/tutorials/understanding-can-a-beginners-guide-to-the-controller-area-network-protocol)

## Стандарты логического уровня

- SAE О1939
- OBD2
- CANopen
- CAN FD
- UDS
- NMEA 2000
- CCP/XCP on CAN

> https://www.csselectronics.com/pages/can-bus-simple-intro-tutorial

## Кодирование данных

![width:20cm center](./images/differential-mode-signal.png)

Уровень dominant имеет приоритет над уровнем recessive

> [circuitbread](https://www.circuitbread.com/tutorials/understanding-can-a-beginners-guide-to-the-controller-area-network-protocol)

## Приём данных

![width:20cm center](./images/CAN-bus-Bit-Timing.png)

> https://autotechdrive.com/can-bus-guide/

## Помехозащищённость

![width:20cm center](./images/noise-immunity.png)

> [circuitbread](https://www.circuitbread.com/tutorials/understanding-can-a-beginners-guide-to-the-controller-area-network-protocol)

## Добавление битов

![height:12cm center](./images/bit-stuffing.png)

После 5 одинаковых битов подряд добавляется один противоположный бит

> [circuitbread](https://www.circuitbread.com/tutorials/understanding-can-a-beginners-guide-to-the-controller-area-network-protocol)

## Арбитраж

![height:12cm center](./images/can-arbitration.png)

Приоритет у сообщения с меньшим ID

> [circuitbread](https://www.circuitbread.com/tutorials/understanding-can-a-beginners-guide-to-the-controller-area-network-protocol)

## Формат данных I

![width:100% bg right:40%](./images/Data-frame-1024x576.png)

- На шине без передачи данных `1`
- Start of Frame: бит синхронизации `0`
- Identifier: идентификатор и приоритет сообщения
- Remote Frame: бит запроса данных
- Control Field: информация о размере данных

> https://autotechdrive.com/can-bus-guide/

## Формат данных II

![width:100% bg right:40%](./images/Data-frame-1024x576.png)

- Data Field: данные
- CRC Field: контрольная сумма
- ACK Slot: подтверждение от приёмника
- End of Frame: конец сообщения (7 `1`)
- Inter Frame Space: промежуток между сообщениями (7 `1`)

> https://autotechdrive.com/can-bus-guide/

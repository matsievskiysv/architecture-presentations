---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Joint Test Action Group: JTAG

Микропроцессорные системы

## Проблемы с производством

![height:12cm bg right:60%](./images/mount_faults.png)

- Отсутствие контакта
- Закороченные контакты

> [Corelis JTAG tutorial](https://www.corelis.com/educationdownload/JTAG-Tutorial.pdf)

## JTAG

![width:20cm center](./images/timeline.png)

- Стандарт тестирования печатных плат
- Позволяет использовать внутренние ресурсы устройства для тестирования и отладки

> [Corelis JTAG tutorial](https://www.corelis.com/educationdownload/JTAG-Tutorial.pdf)

## Принцип тестирования

![height:10cm center](./images/test_example.png)

> [Corelis JTAG tutorial](https://www.corelis.com/educationdownload/JTAG-Tutorial.pdf)

## Соединение устройств

| ![height:8cm center](./images/daisy_chain.png)  | ![height:8cm center](./images/daisy_chain_internal.png)  |
|:-:|:-:|
|   |   |

> [Corelis JTAG tutorial](https://www.corelis.com/educationdownload/JTAG-Tutorial.pdf)

## Взаимодействие с устройствами I

![width:100% bg right:40%](./images/test_example_complex.png)

- Эмуляция взаимодействия по простым шинам

> [Corelis JTAG tutorial](https://www.corelis.com/educationdownload/JTAG-Tutorial.pdf)

## Взаимодействие с устройствами II

![width:100% bg right:40%](./images/flash_programming.png)

- Загрузка данных в память
- Загрузка алгоритма в память
- Выполнение алгоритма

> [Corelis JTAG tutorial](https://www.corelis.com/educationdownload/JTAG-Tutorial.pdf)

## Выводы шины

![width:100% bg right:40%](./images/structure.png)

- `TCK` (test clock): Синхроимпульс
- `TMS` (test mode select): Выбор режима
- `TDI` (test data in): вход данных
- `TDO` (test data out): выход данных
- `TRST` (test reset): сброс теста
- `NRST` (reset): сброс устройства

> [Corelis JTAG tutorial](https://www.corelis.com/educationdownload/JTAG-Tutorial.pdf)

## JTAG адаптер

![height:12cm center](./images/debug-setup-with-ft2232hl-serial-and-segger-j-link-edu-mini.png)

> [JTAG Debugging the ESP32 With FT2232 and OpenOCD](https://dzone.com/articles/jtag-debugging-the-esp32-with-ft2232-and-openocd)

## Регистры JTAG

- Используются 2 регистра
   - Регистр команды (IR): выбор команды
   - Регистр данных (DR): аргумент команды
- Могут быть различной длины
- Реализованы в виде сдвиговых регистров: старые данные выталкиваются новыми

## Машина состояния JTAG

![width:100% bg right:50%](./images/state_machine.png)

- `1` и `0` в машине состояний -- значения сигнала `TMS`
- Сброс состояния в `Test-Logic-Reset` достигается заданием `TMS=1` и 5 тактовыми импульсами

> [Corelis JTAG tutorial](https://www.corelis.com/educationdownload/JTAG-Tutorial.pdf)

## Загрузка команды JTAG I

![width:100% bg right:50%](./images/state_machine.png)

- `Test-Logic-Reset`
- `Select IR Scan`
- `Capture IR`
- `Shift IR`, задавая данные на входе
- `Exit IR`
- `Update IR`

> [Corelis JTAG tutorial](https://www.corelis.com/educationdownload/JTAG-Tutorial.pdf)

## Загрузка команды JTAG II

![height:10cm center](./images/JTAG7.gif)

> [FPGA4Fun](https://www.fpga4fun.com/JTAG2.html)

## Команды JTAG

- `BYPASS`
   - код -- все `1`
   - соединение `TDI` и `TDO`
   - задержка на один период; передаётся бит `0`
- `IDCODE`
   - нет стандартного значения
   - выдаёт 32 битный ID устройства
   - выбирается при переходе к `Test-Logic-Reset`
   - `0` если не реализована
   - последний бит `1` если реализована
   - не обязательна, но обычно реализована

## Поиск устройств на шине JTAG

- Задать достаточное количество `1` в `IR`
- Применить команду, все устройства входят в режим `BYPASS`
- Передать `0` в `DR` и посчитать задержку
- Считать ID устройств

## BSDL файл

```vhdl
attribute INSTRUCTION_LENGTH of EP1C3T100 : entity is 10;

attribute INSTRUCTION_OPCODE of EP1C3T100 : entity is
  "BYPASS            (1111111111), "&
  "EXTEST            (0000000000), "&
  "SAMPLE            (0000000101), "&
  "IDCODE            (0000000110), "&
  "USERCODE          (0000000111), "&
  "CLAMP             (0000001010), "&
  "HIGHZ             (0000001011), "&
  "CONFIG_IO            (0000001101)";

attribute INSTRUCTION_CAPTURE of EP1C3T100 : entity is "0101010101";

attribute IDCODE_REGISTER of EP1C3T100 : entity is
  "0000"&               --4-bit Version
  "0010000010000001"&   --16-bit Part Number (hex 2081)
  "00001101110"&        --11-bit Manufacturer's Identity
  "1";                  --Mandatory LSB

attribute BOUNDARY_LENGTH of EP1C3T100 : entity is 339;
```

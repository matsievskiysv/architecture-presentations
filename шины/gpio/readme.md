---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# GPIO и SGPIO

Микропроцессорные системы

## Режимы работы

- Выход
  - Push-pull
  - С открытым коллектором (open drain)
- Вход
  - Без подтягивающих резисторов
  - Без подтягивающим резистором к земле/питанию (pull down/ pull up)
- Не подключён (HiZ)
- Альтернативная функция

*Ток выводов GPIO ограничен*

## Выход в режиме Push-Pull

![height:10cm center](./images/push-pull.png)

## Выход в режиме с открытым коллектором I

![height:10cm center](./images/open-drain.png)

Внутренний подтягивающий резистор

## Выход в режиме с открытым коллектором II

![height:10cm center](./images/open-drain-ext.png)

Внешний подтягивающий резистор

## Вход

![height:10cm center](./images/pull-up-down.png)

## Треск

![height:10cm center](./images/bounce.png)

> http://www.labbookpages.co.uk/

## Общая схема

![height:10cm center](./images/gpio-structure.jpg)

> STM32F401 manual

## Bitbang

* PWM
* SPI
* I2C
* SGPIO

## Прерывания

- По фронту
- По спаду

![right bg height:10cm](./images/edge.jpg)

## Датчик угла поворота

![height:10cm center](./images/QuadratureAnimation.gif)

> https://www.allaboutcircuits.com

## Определение направления движения

1. Прерывание по спаду вывода `PinA`
2. Чтение значения на выводе `PinB`
   - если `1`, то движение по часовой стрелке
   - если `0`, то движение против часовой стрелки

![right bg height:10cm](./images/encoder_rotation.png)

## Широтно-импульсная модуляция

![height:10cm center](./images/pwm.png)

> http://www.siriusmicro.com/

## Сдвиговый регистр I

![height:10cm center](./images/74hc595.png)

> 74HC595 datasheet

## Сдвиговый регистр II

![height:10cm center](./images/shift_register.gif)

## SGPIO

![height:10cm center](./images/sgpio.png)

---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# 1 Wire

Микропроцессорные системы

## 1 Wire

![width:100% bg right:40%](./images/ibutton.jpg)

- Шина со многими ведомыми устройствами в пределах одной или смежных плат
- Данные передаются по шине питания
- Низкая скорость передачи данных
- Используется для простых датчиков
- Возможность подключения устройств при помощи контакта с металлическим корпусом: iButton

> https://en.wikipedia.org/wiki/1-Wire

## Посылка данных

![height:12cm center](./images/transaction.png)

> [1wire standard](https://en.wikipedia.org/wiki/1-Wire#cite_note-maximic_1wire_standard-7)

## Сигнал reset

![width:20cm center](./images/reset.png)

> [Microchip AN1199](https://ww1.microchip.com/downloads/en/appnotes/01199a.pdf)

## Передача бита

![width:20cm center](./images/write.png)

> [Microchip AN1199](https://ww1.microchip.com/downloads/en/appnotes/01199a.pdf)

## Чтение бита

![width:20cm center](./images/read.png)

> [Microchip AN1199](https://ww1.microchip.com/downloads/en/appnotes/01199a.pdf)

## Обращение к устройству

- Все устройства имеют уникальный 64-битный номер ID
- Команда `MATCH ROM` (`0x55`): выбор устройства по ID
- Команда `READ ROM` (`0x33`): выбор единственного устройства на шине
- Команда `SEARCH ROM` (`0xf0`): поиск ID устройств на шине

## Поиск устройств

- Команда `SEARCH ROM` от ведущего
- От всех ведомых: 1й бит адреса
- От всех ведомых: инвертированный 1й бит адреса
- 1й бит адреса от ведущего
- ...
- От всех ведомых: 64й бит адреса
- От всех ведомых: инвертированный 64й бит адреса

## Поиск устройств

| Бит N | Бит ̅N | Значение |
|:-:|:-:|:-:|
| `0` | `1` | Бит адреса устройств `0` |
| `1` | `0` | Бит адреса устройств `1` |
| `0` | `0` | Биты адреса устройств `0` и `1` |
| `1` | `1` | Нет устройств с таким адресом  |

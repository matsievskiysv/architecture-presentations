<!-- % Микропроцессорные системы -->
<!-- pandoc -f markdown -t pdf --pdf-engine=wkhtmltopdf --pdf-engine-opt=--page-size --pdf-engine-opt=a4 --pdf-engine-opt=--minimum-font-size --pdf-engine-opt=20 --pdf-engine-opt=--page-offset --pdf-engine-opt=1 --pdf-engine-opt=-q -s -o readme.pdf readme.md -->

# Шины передачи данных
## Главы

1. [Pin control](./pinctrl)
1. [GPIO и SGPIO](./gpio)
1. [SPI](./spi)
1. [UART](./uart)
1. [I2C](./i2c)
1. [1-Wire](./1wire)
1. [CAN](./can)
1. [JTAG](./jtag)
1. [USB](./usb)
1. MODBUS
1. Ethernet
1. SATA
1. PCIe

# Лабораторные работы

1. [GPIO](./lab-gpio)
1. [GPIO bitbang](./lab-gpio-bitbang)
1. [UART](./lab-uart)
1. [1-Wire](./lab-1wire)

## Литература

1. [Pin control](./pinctrl)
	* https://www.kernel.org/doc/html/latest/driver-api/pin-control.html
1. [GPIO и SGPIO](./gpio)
	* https://www.wikipedia.org/
1. [SPI](./spi)
	* https://www.corelis.com/
1. [UART](./uart)
	* https://www.wikipedia.org/
1. [I2C](./i2c)
	* https://www.wikipedia.org/
1. [1-Wire](./1wire)
	* [Microchip AN1199](https://ww1.microchip.com/downloads/en/appnotes/01199a.pdf)
1. [CAN](./can)
	* https://www.csselectronics.com/pages/can-bus-simple-intro-tutorial
	* https://www.allaboutcircuits.com/technical-articles/introduction-to-can-controller-area-network/
	* https://www.circuitbread.com/tutorials/understanding-can-a-beginners-guide-to-the-controller-area-network-protocol
	* https://autotechdrive.com/can-bus-guide/
1. [JTAG](./jtag)
	* https://www.corelis.com/educationdownload/JTAG-Tutorial.pdf
	* https://www2.lauterbach.com/pdf/training_jtag.pdf
	* https://wrongbaud.github.io/posts/jtag-hdd/
	* https://www.fpga4fun.com/JTAG1.html
1. [USB](./usb)
	* [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb1.shtml)
	* [Cypress AN57294](https://www.infineon.com/dgdl/Infineon-AN57294_USB_101_An_Introduction_to_Universal_Serial_Bus_2.0-ApplicationNotes-v09_00-EN.pdf?fileId=8ac78c8c7cdc391c017d072d8e8e5256)
	* [USB 2.0 specification](https://www.usb.org/document-library/usb-20-specification)
	* [USB CDC specification](https://www.usb.org/document-library/class-definitions-communication-devices-12)
	* [Silicon Labs AN571](https://www.silabs.com/documents/public/application-notes/AN571.pdf)

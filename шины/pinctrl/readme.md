---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Альтернативные функции выводов

Микропроцессорные системы

## Выводы ATTiny

![height:12cm center](./images/attiny.gif)

## Альтернативные функции выводов

| ![height:10cm center](./images/hw-cent-control.svg)  | ![height:10cm center](./images/hw-dist-control.svg)  |
|:-:|:-:|
|   |   |

> https://docs.zephyrproject.org/

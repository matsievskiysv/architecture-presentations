---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Universal Serial Bus: USB

Микропроцессорные системы

## USB

- Шина передачи данных общего назначения со многими ведомыми устройствами
- Все коммуникации инициируются ведущим устройством
- Дифференциальная передача данных и контрольные суммы
- Древовидная топология соединения
- Авто-детектирование подключения
- Предоставление питания
- Многоуровневый протокол передачи данных
- Поддержка многих конфигураций и функций на одной линии

> https://en.wikipedia.org/wiki/USB

## Версии стандарта

| Стандарт  | Год  | Скорость  | Ток, [А]  | Разъём  |
|:-:|:-:|:-:|:-:|:-:|
| 1.0  | 1996  | 1,56 МБ/с  | 0,1  | Type A, Type B  |
| 1.1  | 1998  | 12 МБ/с  | 0,1  |  |
| 2.0  | 2000  | 480 МБ/с  | 0,5  | Mini |
| 2.0  | 2007  | 480 МБ/с  | 0,5  | Micro |
| 3.0  | 2008  | 5 ГБ/с  | 0,9  | Super Speed |
| 3.1  | 2013  | 10 ГБ/с  | 1,5  |  |
| 3.1  | 2014  | 10 ГБ/с  | 3  | Type C |
| 3.2  | 2017  | 20 ГБ/с  | 5  |  |
| 4.0  | 2019  | 40 ГБ/с  | 5  |  |

## Топология соединения

![height:12cm center](./images/Universal-Serial-Bus-USB.jpg)

## Идентификация подключения

| ![width:15cm center](./images/lspullup.gif)  | ![width:15cm center](./images/fspullup.gif)  |
|:-:|:-:|
| Low speed | Full speed |

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb2.shtml)

## Уровни на шине USB

- Дифференицальная `1`: `D+` = `1`, `D-` = `0`
- Дифференицальный `0`: `D+` = `0`, `D-` = `1`
- Униполярная `1` (`SE1`): `D+` = `1`, `D-` = `1`
- Униполярный `0`(`SE0`): `D+` = `0`, `D-` = `0`

![width:100% bg right:35%](./images/comm.png)

> [Cypress AN57294](https://www.infineon.com/dgdl/Infineon-AN57294_USB_101_An_Introduction_to_Universal_Serial_Bus_2.0-ApplicationNotes-v09_00-EN.pdf?fileId=8ac78c8c7cdc391c017d072d8e8e5256)

## Состояния шины USB

<div class="twocols">

- Idle: до и после передачи данных состояние `J`
- Resume: состояние `K`
- Start of packet (`SOF`): `J`->`K`
- End of packet (`EOP`): `SE0`(2б)->`J`(1б)
- Reset: `SE0`(10 мс)

<p class="break"></p>

|   | J  | K  |
|:-|:-:|:-:|
| Low  | `0`  | `1`  |
| Full  | `1`  | `0`  |
| High  | `1`  | `0`  |

</div>

> [Cypress AN57294](https://www.infineon.com/dgdl/Infineon-AN57294_USB_101_An_Introduction_to_Universal_Serial_Bus_2.0-ApplicationNotes-v09_00-EN.pdf?fileId=8ac78c8c7cdc391c017d072d8e8e5256)

## Питание устройств

- При подключении устройство может потреблять 0,1 А
- При дальнейшей конфигурации устройство может запросить больший ток
- В режиме Suspend питание не должно превышать 0,5 мА на единицу потребления
- Устройство входит в режим Suspend при отсутствии активности в течение 3 мс

## Типы передачи данных

- Control: запросы описателей, конфигурация устройства
- Interrupt: периодический опрос устройств с заданным периодом
- Bulk: передача данных без потерь без заданной скорости
- Isochronous: передача данных с заданной скоростью с возможными потерями части данных

## Адресация устройств

![width:100% bg right:42%](./images/endpoint.gif)

- Function: конечные устройства
- Endpoint (EP): источник/приёмник данных
- Pipe:
  - Message: двунаправленная передача для Control
  - Stream: однонаправленная передача
- Все устройства должны иметь `EP0`

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb2.shtml)

## Передача пакетов данных

- Token: управляющий заголовок
- Data: передаваемые данные (могут отсутствовать)
- Status: статус передачи

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb3.shtml)

## Пакеты данных Token

`Sync|PID|Addr|Endp|CRC5|EOP`

- In: запрос на чтение данных
- Out: запрос на запись данных
- Setup: управляющие команды


> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb3.shtml)

## Поля пакетов данных

- Sync: синхронизация приёмника
- PID: тип пакета
  - формат поля `PID0|PID1|PID2|PID3|nPID0|nPID1|nPID2|nPID3`
- Addr: адрес устройства
- Endp: номер endpoint
- CRC: контрольная сумма
- EOP: конец пакета

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb3.shtml)

## Пакеты данных Data

`Sync|PID|DATA|CRC16|EOP`

| Тип устройства  | Максимальный размер  |
|:-|:-:|
| Low  | 8  |
| Full  | 1023  |
| High  | 1024  |


> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb3.shtml)

## Пакеты данных Handshake

`Sync|PID|EOP`

- ACK: подтверждение передачи
- NAK:
  - устройство временно не может передавать данные
  - данные отсутствуют в режиме Interrupt
- STALL: требуется действие ведущего устройства


> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb3.shtml)

## Пакеты данных Start of Frame

`Sync|PID|Frame Number|CRC5|EOP`

Периодически посылается на High и Full speed устройствах

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb3.shtml)

## Control transfer

- Setup stage: указание команды
- Data stage: передача данных
- Status stage: подтверждение передачи

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb4.shtml)

## Control transfer: Setup stage

| ![width:15cm center](./images/contset.gif)  | ![width:8cm center](./images/transkey.gif)  |
|:-:|:-:|
| | |

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb4.shtml)

## Control transfer: стандартные запросы

- Задание адреса
- Запрос дескрипторов/статуса
- Запрос/задание конфигурации

> [USB 2.0 specification](https://www.usb.org/document-library/usb-20-specification)

## Control transfer: Data stage

| ![width:15cm center](./images/contdata.gif)  | ![width:8cm center](./images/transkey.gif)  |
|:-:|:-:|
| | |

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb4.shtml)

## Control transfer: Status stage

| ![width:15cm center](./images/contsta1.gif)  | ![width:8cm center](./images/transkey.gif)  |
|:-:|:-:|
| ![width:15cm center](./images/contsta2.gif) | |

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb4.shtml)

## Interrupt transfer

| ![width:15cm center](./images/transint.gif)  | ![width:8cm center](./images/transkey.gif)  |
|:-:|:-:|
|  | |

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb4.shtml)

## Bulk transfer

| ![width:10cm center](./images/contdata.gif)  | ![width:8cm center](./images/transkey.gif)  |
|:-:|:-:|
|  | |

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb4.shtml)

## Isochronous transfer

| ![width:10cm center](./images/transiso.gif)  | ![width:8cm center](./images/transkey.gif)  |
|:-:|:-:|
|  | |

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb4.shtml)

## USB дескрипторы

![height:12cm center](./images/desctree.gif)

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb5.shtml)

## Конфигурации

![height:12cm center](./images/intftree.gif)

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb5.shtml)

## Запрос дескрипторов

![height:12cm center](./images/confsize.gif)

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb5.shtml)

## Описатель устройства

- Версия USB
- Информация о производителе, устройстве
   - idVendor
   - idProduct
   - bDeviceClass
- Количество конфигураций

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb5.shtml)

## Описатель конфигурации

- Количество интерфейсов
- Ток потребления
- Атрибуты

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb5.shtml)

## Описатель интерфейса

- Количество endpoint
- Класс, подкласс, протокол интерфейса

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb5.shtml)

## Описатель endpoint

- Тип
- Направление
- Размер посылки
- Периодичность опроса
- Атрибуты

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb5.shtml)

## Описатель строк

- Поддерживаемые языки

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb5.shtml)

## Строка

- Размер символа: 2 байта
- Кодировка: UTF-8

> [USB in a NutShell](https://beyondlogic.org/usbnutshell/usb5.shtml)

## CDC последовательный порт

- Установить
   - idVendor, idDevice
   - класс, подкласс устройства
   - класс, подкласс интерфейса
- Настроить
   - interrupt, bulk endpoint
- Реализовать 2 обязательные команды

> USB Class Definitions for Communications Devices

## USB нумерация

1. Детектирование присутствия
1. Детектирование скорости при помощи состояний `JK`
1. Считывание дескриптора устройства
1. Задание адреса устройств
1. Считывание дескрипторов
1. Выбор конфигурации

> [Cypress AN57294](https://www.infineon.com/dgdl/Infineon-AN57294_USB_101_An_Introduction_to_Universal_Serial_Bus_2.0-ApplicationNotes-v09_00-EN.pdf?fileId=8ac78c8c7cdc391c017d072d8e8e5256)

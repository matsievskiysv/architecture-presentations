---
marp: true
theme: default
headingDivider: 2
paginate: true
footer: Микропроцессорные системы
size: 16:9
style: |
  section {
  padding-top: 80px;
  }
  h2 {
  position: absolute;
  left: 40px;
  top: 40px;
  }
  blockquote {
  font-size: 60%;
  margin-top: 50px;
  }
  table, th, td {
  border: none!important;
  }
  table {
  margin: 0 auto;
  }
  img[alt~="center"] {
  display: block;
  margin: 0 auto;
  }
  div.twocols {
  margin-top: 35px;
  column-count: 2;
  }
  div.twocols p:first-child,
  div.twocols h1:first-child,
  div.twocols h2:first-child,
  div.twocols ul:first-child,
  div.twocols ul li:first-child,
  div.twocols ul li p:first-child {
  margin-top: 0 !important;
  }
  div.twocols p.break {
  break-before: column;
  margin-top: 0;
  }
---

<!-- marp --html readme.md -o readme.html -->

# Сети: канальный уровень

Микропроцессорные системы

## Уровни абстракции

![](./images/models.png)

> Drew Saunders: Networking Systems

## Ethernet: thicknet

![](./images/ethernet-1.png)

> Kenneth Castelino: Ethernet

## Ethernet: thinnet

![](./images/ethernet-2.png)

> Kenneth Castelino: Ethernet

## Ethernet: twisted pair

![](./images/ethernet-3.png)

> Kenneth Castelino: Ethernet

## Ethernet: оборудование

| ![](./images/ethernet-devices-1.png) | ![](./images/ethernet-devices-2.png) |
|:-:|:-:|
| | |

> Kenneth Castelino: Ethernet

## Hub vs Switch

| ![](./images/hub.jpeg) | ![](./images/switch.jpeg) |
|:-:|:-:|
| Концентратор (Hub) | Коммутатор (Switch) |

## Hub vs Switch

| Hub | Switch |
|:-:|:-:|
| Физический уровень | Коммутационный уровень |
| Передача только всем | Передача одному, нескольким, всем |
| Общий домен коллизий | Устройств изолированы |
| Нет фильтрации пакетов | Возможна фильтрация пакетов |
| Небезопасный | Относительно безопасный |
| Дешёвый | Относительно дорогой |

## Router

- Оперирует на сетевом уровне
- Поддерживает сетевые протоколы
- Включает в себя функции коммутатора

## Принципы протоколов связи

Протоколы – наборы правил

- Приложение: что сделать
* Адресация: куда идут данные
* Среда передачи: как передаются данные
* Подтверждение передачи: пришли ли данные

> Drew Saunders: Networking Systems

## Вложенность протоколов

![bg right width:100%](./images/packet.png)

- Пользователю интересны данные самого высоко уровня
* Данные высоких уровней вкладываются в пакеты низких уровней

> https://scapy.readthedocs.io/en/latest/usage.html

## Кадр Ethernet

![bg right width:80%](./images/ethernet-frame.jpg)

- Preamble, SFD: синхронизация устройств
* Source, destination MAC: адреса устройств
* Ethernet header: дополнительные параметры
* Data: передаваемые данные
* FCS: контрольная сумма
* IFG: задержка между кадрами

> https://en.wikipedia.org/wiki/Ethernet_frame

## Адресация Ethernet

- 6 байт (например, `02:42:10:45:3d:8a`)
* Уникальный для каждого интерфейса
* По первым трём байтам можно узнать производителя
* Половина адресов используется для протоколов (Например, `01:19:A7:00:00:[Ring ID]` для ERPS)
* Адрес `ff:ff:ff:ff:ff:ff` используется для рассылки пакета всем устройствам в сети (например, протокол ARP)

## Протокол ARP

ARP: Address resolution protocol

- Используется для связки сетевого и коммутационного уровней
* Предназначен для определения MAC адреса по IP адресу

Алгоритм:
* Устройство посылает ARP-пакет с целевым IP адресом на MAC адрес `ff:ff:ff:ff:ff:ff`
* Другие устройства сравнивают IP со своим
* При совпадении IP адреса отсылается посылка с MAC адресом устройства

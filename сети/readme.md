<!-- % Микропроцессорные системы -->
<!-- pandoc -f markdown -t pdf --pdf-engine=wkhtmltopdf --pdf-engine-opt=--page-size --pdf-engine-opt=a4 --pdf-engine-opt=--minimum-font-size --pdf-engine-opt=20 --pdf-engine-opt=--page-offset --pdf-engine-opt=1 --pdf-engine-opt=-q -s -o readme.pdf readme.md -->

# Сети
## Главы

1. [Канальный уровень](./канальный_уровень)
	1. Уровни абстракции
	1. Принципы протоколов связи. Вложенность протоколов
	1. Хабы и коммутаторы
	1. Кадр Ethernet
	1. Адресация Ethernet
	1. Ethernet broadcast
	1. Протокол ARP
1. Сети: сетевой уровень
	1. IP
	1. NAT
	1. Firewall

## Литература

18. [Канальный уровень](./канальный_уровень)
	* https://www.wikipedia.org/
